<?php 
/**
 * Profile template 
 *
 * Used to display the profile page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

?>
          <?php $user = get_user_single($_SESSION['userid']); ?>

          <h1 class="page-header">My Profile</h1>

          <div class="row">
            <div class="col-lg-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-user"></i> Update Your Profile</h3>
                </div>
                <div class="panel-body">

                    <form>

                      <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo htmlspecialchars($user['first_name']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" class="form-control" id="last_name" id="last_name" value="<?php echo htmlspecialchars($user['last_name']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="user_email" id="user_email" value="<?php echo htmlspecialchars($user['user_email']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Phone Number</label>
                        <input type="phone" class="form-control" id="user_phone" id="user_phone" value="<?php echo htmlspecialchars($user['user_phone']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Current Password</label>
                        <input type="email" class="form-control" id="current_pass">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">New Password</label>
                        <input type="password" class="form-control" id="new_pass">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Confirm New Password</label>
                        <input type="password" class="form-control" id="confirm_pass">
                      </div>

                      <button type="submit" class="btn btn-primary">Update</button>

                    </form>

                </div><!-- end panel body -->
              </div><!-- end panel -->
            </div><!-- end col-6 -->

            <div class="col-lg-6">
              <div class="panel panel-default">

                <div class="panel-body">
                  <p class="bold">My Gravatar</p>

                  <img id="display-gravatar" src="<?php echo htmlspecialchars($user['user_gravatar']) . '?s=140&amp;d=mm&amp;r=g'; ?>">

                </div><!-- end panel-body -->
              
              </div><!-- end panel -->
            </div><!--end col-lg-6 -->
          </div><!-- end row -->

<?php include('dashboard-footer.php'); ?>