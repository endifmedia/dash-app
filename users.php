<?php 
/**
 * Users template 
 *
 * Used to display the users page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 superuser_only($_SESSION['userid']);
  
?>
       
          <h1 class="page-header">Stringcan Users <a href="user-new.php" class="btn btn-default pull-right"><i class="fa fa-plus"> Add New</i></a></h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Gravatar</th>
                  <th>Name</th>
                  <th>Team</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Active</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php 
                  $users = get_users();
                  //$n = 0;
                  $html = '';


                  foreach ($users as $user) {

                    $html .= '<tr>';

                      $html .= '<td><img src="' . htmlspecialchars($user['user_gravatar']) . '?s=35&d=mm&r=g"></td>';
                      $html .= '<td>' . htmlspecialchars($user['first_name']) . ' ' . $user['last_name'] . '</td>';
                      $html .= '<td>' . htmlspecialchars($user['user_team']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($user['user_email']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($user['user_phone']) . '</td>';
                      
                      if($user['account_status'] == 'active'){

                        $html .= '<td><i class="fa fa-check active"></i></td>';

                      } else {

                        $html .= '<td><i class="fa fa-times suspended"></i>'; 

                      }
                      $html .= '<td>';
                        $html .= '<a href="edit-user.php?customer_id=' . htmlspecialchars($user['ID']) . '" id="edit_user"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" title="edit"> Edit</i></button></a> '; 
                        $html .= '<a href="#" id="delete_user"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-times" title="delete"> Delete</i></button></a> ';
                        //$html .= '<a href="client.php?customer_id=' . htmlspecialchars($user['ID']) . '" id="view_user"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-tachometer" title="delete"> View Dash</i></button>';
                      $html .= '</td>';

                    $html .= '</tr>'; 

                  }

                  echo $html; 

              ?>

              </tbody>
            </table>
          </div><!-- end table-responsive -->

<?php include('dashboard-footer.php'); ?>