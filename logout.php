<?php
/**
 * File that controls user logout
 * 
 * @version 1.1 dashboard app
 */

include('functions.php');

log_out();