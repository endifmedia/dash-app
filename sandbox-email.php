<?php 
/**
 * Sandbox template 
 *
 * Used to display the sandbox overview page.
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

?>
          
          <h1 class="page-header">Sandbox - Emails</h1>
        
           <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>     
                  <th>Subject</th> 
                  <th># of recipients</th>   
                  <th># Of clicks</th>
                  <th># Of reads</th>
                  <th>Date Sent</th>
                </tr>
              </thead>
              <tbody>

	             <?php 

		          	$response = get_sandbox(); 

		          	foreach($response->Broadcasts->Broadcast as $sandbox) {

		          		echo '<tr>';

		          			//echo '<td>' . $sandbox->Status . '</td>';
		          			echo '<td>' . $sandbox->Subject . '</td>';
		          			echo '<td>' . $sandbox->NumberOfRecipients. '</td>';
		          			echo '<td>' . $sandbox->NumberOfClicks . '</td>';
		          			echo '<td>' . $sandbox->NumberOfReads . '</td>';
							echo '<td>' . date("M n, Y", strtotime($sandbox->DatetimeSent)) . '</td>';		          			

		          		echo '</tr>';

		          	}
          		?>

              </tbody>
            </table>
          </div>

<?php include('dashboard-footer.php'); ?>