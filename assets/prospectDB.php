<?php
/**
 * Prospect Database connection template 
 *
 * Used to connect to the prospect database 
 * @version 1.1 dashboard app
 */
/*hostname = (server ip)
Database name = (cpanelUsername_databaseName)
Database username = (cpanelUsername_databaseUsername)
Database password = (the password you entered for that database user)
MySQL Connection Port = 3306
TCP or UDP, either is fine.*/

try {

  $db = new PDO("mysql:host=IPaddress;dbname=databaseName;port=3306","username", "password");
  //var_dump
  $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $db->exec("SET NAMES 'utf8'");

} catch (Exception $e) {

  echo $e;
  exit;

}