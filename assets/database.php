<?php 
/**
 * Database connection template 
 *
 * Used to connect to the database 
 * @version 1.1 dashboard app
 */

try {

  $db = new PDO("mysql:host=localhost;dbname=dash_app;port=8888","root", "root");
  //var_dump
  $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $db->exec("SET NAMES 'utf8'");

} catch (Exception $e) {

  echo $e;
  exit;

}