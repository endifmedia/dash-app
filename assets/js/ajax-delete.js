$(document).ready(function() {
  $('a.delete').click(function(e) {
    e.preventDefault();
    var parent = $(this).parent().parent();
    var id = $(this).parent().val();
    $.ajax({
      type: 'get',
      url: 'delete.php',
      data: 'ajax=1&id=' + id,
      beforeSend: function() {
        //parent.animate({'background':'#fb6c6c'},1000);
        //$('#1 > td').css('backgroundColor', '#fb6c6c' ).animate(300);
        parent.children().css('backgroundColor', 'rgb(219, 115, 115)' ).animate(300);
      },
      success: function() {
        //parent.css('backgroundColor', '#fb6c6c' ).animate(300);
        parent.slideUp(1000,function() {
          parent.remove();
        });
      }
    });
  });
});