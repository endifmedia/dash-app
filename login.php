<?php

  include('functions.php');

    if(isset($_POST['submit'])){

      if ($_POST['email'] != '' && $_POST['password'] != '') {
        
           // Load code below if both username
           // and password submitted are correct
          $user_email = htmlspecialchars(trim($_POST['email']));
          $password = htmlspecialchars(trim($_POST['password']));
    
          // Redirect to a protected page
          verify_user($user_email, $password);
   
      }

    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Sign in &middot; DASH.app</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <!-- <link href="assets/css/bootstrap.css" rel="stylesheet"> -->
    <link href="dash-bootstrap.min.css" rel="stylesheet">    
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <style type="text/css">

      .alert{
        text-align: center;
        font-size: 14px;
      }
      .animated {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }

      @-webkit-keyframes shake {
        0%, 100% {
          -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
        }

        10%, 30%, 50%, 70%, 90% {
          -webkit-transform: translate3d(-10px, 0, 0);
          transform: translate3d(-10px, 0, 0);
        }

        20%, 40%, 60%, 80% {
          -webkit-transform: translate3d(10px, 0, 0);
          transform: translate3d(10px, 0, 0);
        }
      }

      @keyframes shake {
        0%, 100% {
          -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
        }

        10%, 30%, 50%, 70%, 90% {
          -webkit-transform: translate3d(-10px, 0, 0);
          transform: translate3d(-10px, 0, 0);
        }

        20%, 40%, 60%, 80% {
          -webkit-transform: translate3d(10px, 0, 0);
          transform: translate3d(10px, 0, 0);
        }
      }

      .shake {
        -webkit-animation-name: shake;
        animation-name: shake;
      }
      
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      h2 {
        font-family: Impact;
        color: #333;
        font-size: 1.9em;
        text-align: center;
      }
      span.dash {
        font-size: 39px;
        color: #333;
        padding-left: 5px;
        border-top-right-radius: 4px;
      }
      span.dot {
        padding: 1px;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        margin-top: 7em;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
  </head>

  <body>

    <div class="container<?php if(isset($GLOBALS['error']['class'])){ echo htmlspecialchars($GLOBALS['error']['class']); } ?>">

      <form class="form-signin" method="post">
        <h2 class=""><i class="fa fa-tachometer"></i> <span class="dash">DASH</span><span class="dot">.</span><span class="app">app</span></h2>
        <?php if(isset($GLOBALS['msg']['login-err'])){ ?>
          <p class="alert<?php echo htmlspecialchars($GLOBALS['msg']['class']); ?>"><?php echo htmlspecialchars($GLOBALS['msg']['login-err']); ?></p>
        <?php } ?>
        <input type="text" id="email" name="email" class="input-block-level" placeholder="Email address">
        <input type="password" id="password" name="password" class="input-block-level" placeholder="Password">
        <button name="submit" class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
  </body>
</html>
