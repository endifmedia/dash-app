<?php 
/**
 * Analytics template 
 *
 * Used to display the analytics page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

?>

          <h1 class="page-header">Analytics </h1><span><div id="embed-api-auth-container"></div></span>

          <!-- Step 2: Load the library. -->
          <script>
          (function(w,d,s,g,js,fs){
            g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
            js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
            js.src='https://apis.google.com/js/platform.js';
            fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
          }(window,document,'script'));
          </script>

          <div class="row">

            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-body">

                  <p class="title">Top Country<br>
                    <span>By session</span></p>

                  <div id="chart-1-container"></div>
                  <div id="view-selector-container"></div>
                  <div class="view-selector" id="view-selector-1-container"></div>
                  <div id="chart-container"></div>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-body">

                <p class="title">Pageviews <br>
                    <span>By session</span></p>
                  <div id="view-selector-container"></div>
                  <div id="chart-1-container"></div>

                  <div id="chart-2-container"></div>

                  <div class="view-selector" id="view-selector-1-container"></div>
                  <div class="view-selector" id="view-selector-2-container"></div>
                  <div id="chart-container"></div>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

          </div>

          <div class="row">

            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-body">

                <p class="title">Top Browsers<br>
                    <span>By session</span></p>

                  <div id="chart-3-container"></div>
                  <div class="view-selector" id="view-selector-3-container"></div>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-body">

                <p class="title">Top OS<br>
                    <span>By session</span></p>

                  <div id="chart-4-container"></div>
                  <div class="view-selector" id="view-selector-4-container"></div>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

          </div>


    <script>

      gapi.analytics.ready(function() {

          /**
           * Authorize the user immediately if the user has already granted access.
           * If no access has been created, render an authorize button inside the
           * element with the ID "embed-api-auth-container".
           */
          gapi.analytics.auth.authorize({
            container: 'embed-api-auth-container',
            clientid: '<?php get_analytics_id($_SESSION['userid']); ?>',
          });

          /**
           * Create a ViewSelector for the first view to be rendered inside of an
           * element with the id "view-selector-1-container".
           */
          var viewSelector1 = new gapi.analytics.ViewSelector({
            container: 'view-selector-1-container'
          });

          /**
           * Create a ViewSelector for the second view to be rendered inside of an
           * element with the id "view-selector-2-container".
           */
          var viewSelector2 = new gapi.analytics.ViewSelector({
            container: 'view-selector-2-container'
          });

          /**
           * Create a ViewSelector for the second view to be rendered inside of an
           * element with the id "view-selector-2-container".
           */
          var viewSelector3 = new gapi.analytics.ViewSelector({
            container: 'view-selector-3-container'
          });

          /**
           * Create a ViewSelector for the second view to be rendered inside of an
           * element with the id "view-selector-2-container".
           */
          var viewSelector4 = new gapi.analytics.ViewSelector({
            container: 'view-selector-4-container'
          });

          /**
           * Create a ViewSelector for the second view to be rendered inside of an
           * element with the id "view-selector-5-container".
           */
          var viewSelector5 = new gapi.analytics.ViewSelector({
            container: 'view-selector-5-container'
          });

          /**
           * Create a ViewSelector for the second view to be rendered inside of an
           * element with the id "view-selector-6-container".
           */
          var viewSelector6 = new gapi.analytics.ViewSelector({
            container: 'view-selector-6-container'
          });

          // Render both view selectors to the page.
          viewSelector1.execute();
          viewSelector2.execute();
          viewSelector3.execute();
          viewSelector4.execute();
          //viewSelector5.execute();
          //viewSelector6.execute();

          /**
           * Create the first DataChart for top countries over the past 30 days.
           * It will be rendered inside an element with the id "chart-1-container".
           */
          var dataChart1 = new gapi.analytics.googleCharts.DataChart({
            query: {
              metrics: 'ga:sessions',
              dimensions: 'ga:country',
              'start-date': '30daysAgo',
              'end-date': 'yesterday',
              'max-results': 6,
              sort: '-ga:sessions'
            },
            chart: {
              container: 'chart-1-container',
              type: 'PIE',
              options: {
                width: '100%'
              }
            }
          });


          /**
           * Create the second DataChart for session overviews.
           * It will be rendered inside an element with the id "chart-2-container".
           */
          var dataChart2 = new gapi.analytics.googleCharts.DataChart({
            query: {
              metrics: 'ga:sessions',
              dimensions: 'ga:date',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-2-container',
              type: 'LINE',
              options: {
                width: '100%'
              }
            }
          });

          /**
           * Create the third DataChart for top browsers.
           * It will be rendered inside an element with the id "chart-3-container".
           */
          var dataChart3 = new gapi.analytics.googleCharts.DataChart({
            query: {
              metrics: 'ga:sessions',
              dimensions: 'ga:browser',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-3-container',
              type: 'BAR',
              options: {
                width: '100%'
              }
            }
          });

          /**
           * Create the fourth DataChart for top os.
           * It will be rendered inside an element with the id "chart-4-container".
           */
          var dataChart4 = new gapi.analytics.googleCharts.DataChart({
            query: {
              metrics: 'ga:sessions',
              dimensions: 'ga:operatingSystem',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-4-container',
              type: 'COLUMN',
              options: {
                width: '100%'
              }
            }
          });

          /**
           * Create the fifth DataChart for top pageviews.
           * It will be rendered inside an element with the id "chart-5-container".
           */
          var dataChart5 = new gapi.analytics.googleCharts.DataChart({
            query: {
              dimensions: 'ga:pagePath',
              metrics: 'ga:pageviews',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-5-container',
              type: 'BAR',
              options: {
                width: '100%'
              }
            }

          });

          /**
           * Create the sixth DataChart for top referals.
           * It will be rendered inside an element with the id "chart-6-container".
           */
          var dataChart6 = new gapi.analytics.googleCharts.DataChart({
            query: {
              dimensions: 'ga:source',
              metrics: 'ga:pageviews',
              'start-date': '30daysAgo',
              'end-date': 'yesterday'
            },
            chart: {
              container: 'chart-6-container',
              type: 'BAR',
              options: {
                width: '100%'
              }
            }

          });

          /**
           * Update the first dataChart when the first view selecter is changed.
           */
          viewSelector1.on('change', function(ids) {
            dataChart1.set({query: {ids: ids}}).execute();
          });

          /**
           * Update the second dataChart when the second view selecter is changed.
           */
          viewSelector2.on('change', function(ids) {
            dataChart2.set({query: {ids: ids}}).execute();
          });

          /**
           * Update the third dataChart when the second view selecter is changed.
           */
          viewSelector3.on('change', function(ids) {
            dataChart3.set({query: {ids: ids}}).execute();
          });

          /**
           * Update the fourth dataChart when the second view selecter is changed.
           */
          viewSelector4.on('change', function(ids) {
            dataChart4.set({query: {ids: ids}}).execute();
          });

          /**
           * Update the fiftth dataChart when the second view selecter is changed.
           */
          viewSelector5.on('change', function(ids) {
            dataChart5.set({query: {ids: ids}}).execute();
          });

          /**
           * Update the fiftth dataChart when the second view selecter is changed.
           */
          viewSelector6.on('change', function(ids) {
            dataChart6.set({query: {ids: ids}}).execute();
          });

      });

    </script>

<?php include('dashboard-footer.php'); ?>