<?php 
/**
 * Analytics template 
 *
 * Used to display the analytics page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 access_google();

?>


<?php echo date('Y-m-d', strtotime('-2 month'));
 echo date('Y-m-d', strtotime('-1 month'));?>

 <?php //access_google(); ?>

          <h1 class="page-header">Overview </h1><span><div id="embed-api-auth-container"></div></span>

          <div class="row" style="margin-bottom: 3em;">
            <div class="col-sm-4">

              <div class="box" data-percent="73" style="background-color: rgb(247, 153, 28);color: white;padding: 10px;">
                  <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-users"></i>
                </div>
                <div class="pull-right" style="width: 50%;  margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;width: 100%;">1000</span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">visits</span> 
                </div>      
              </div>

            </div>

            <div class="col-sm-4">
              <div class="box" data-percent="73" style="background-color: rgb(240, 91, 79);color: white;padding: 10px;">
                  <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-star"></i>
                </div>
                <div class="pull-right" style="width: 50%;  margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;"><?php alexa_rank('64'); ?></span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">alexa rank</span> 
                </div>      
              </div>
            </div>

            <div class="col-sm-4">
              <div class="box" data-percent="73" style="background-color: rgb(5, 141, 199);color: white;padding: 10px;">
                <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-file-o"></i>              
                </div>
                <div class="pull-right" style="width: 50%;margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;width: 100%;">2.1</span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">pages/visit</span> 
                </div>  
              </div>
            </div>

          </div>

         <!-- <div class="row" style="margin-bottom: 3em;">
            <div class="col-sm-4">
              <div class="box" data-percent="73" style="background-color: rgb(247, 153, 28);color: white;padding: 10px;">
                  <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-users"></i>
                </div>
                <div class="pull-right" style="width: 50%;  margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;width: 100%;">1000</span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">visits</span> 
                </div>
                
              </div>
            </div>

            <div class="col-sm-4">
              <div class="box" data-percent="73" style="background-color: rgb(240, 91, 79);color: white;padding: 10px;">
                
                <div style="width: 50%;">
                  <span style="font-size: 4em;font-weight: 100;"><?php alexa_rank('64'); ?></span><br>
                  <span style="padding-left: 1em;">alexa rank</span> 
                </div>
                <div class="pull-right" style="margin-top: -1.6em;font-size: 4em; ">
                  <i class="fa fa-star"></i>                
                </div>

              </div>
            </div>

            <div class="col-sm-4">
               <div class="box" data-percent="73" style="background-color: rgb(5, 141, 199);color: white;padding: 10px;">
                
                <div style="width: 50%;">
                  <span style="font-size: 4em;font-weight: 100;">2.1</span><br>
                  <span style="padding-left: 1em;">pages/visit</span> 
                </div>
                <div class="pull-right" style="margin-top: -1.6em;font-size: 4em; ">
                  <i class="fa fa-file-o"></i>              
                </div>

              </div>
            </div>

          </div> -->

          <div class="row googleapp">
            <!-- <div class="col-sm-3 ">
              <div class="chart" data-percent="73">
                <span>73%</span> 
              </div>
              <div class="label">New visitors</div>
            </div>-->
            <div class="col-sm-4">
              <div class="chart" data-percent="73">
                <span>73%</span> 
              </div>
              <div class="label">New visitors</div>
            </div>
            <!-- <div class="col-sm-3">
              <div class="chart" data-percent="73">
                <span>73%</span> 
              </div>
              <div class="label">New visitors</div>
            </div> -->
            <div class="col-sm-4">
               <div class="traffic ct-perfect-fourth"></div>
               <div class="label">Traffic Sources</div>
            </div>
            <div class="col-sm-4">
              <div class="chart" data-percent="73">
                <span>73%</span> 
              </div>
              <div class="label">New visitors</div>
            </div> 
          </div>

         <!-- <div class="chart">
            <div class="percentage easyPieChart" data-percent="55"><span>86</span>%<canvas width="220" height="220" style="width: 110px; height: 110px;"></canvas></div>
            <div class="label">New visitors</div>
          </div> -->

<?php include('dashboard-footer.php'); ?>