<?php 
/**
 * Social template 
 *
 * Used to display the social page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

?>
          
          <h1 class="page-header">Social</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Client Name</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Telephone</th>
                  <th>Country</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Facebook</td>
                  <td>201 South Illinois ST, Indianapolis In 47905</td>
                  <td>steve@facebook.com</td>
                  <td>574-870-5221</td>
                  <td>United States</td> 
                  <td>
                    <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" title="edit <?php echo strtolower($page); ?>"> Edit</i></button> 
                    <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-times" title="delete <?php echo strtolower($page); ?>"> Delete</i></button>
                  </td>                
                </tr>
              </tbody>
            </table>
          </div>

<?php include('dashboard-footer.php'); ?>
