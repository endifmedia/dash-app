<?php 
/**
 * Edit User template 
 *
 * Used to display the edit profile page to the superuser account 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 can_user_view_page($_SESSION['userid']);



 if(isset($_POST['update_user'])){

    admin_update_user(htmlspecialchars($_GET['customer_id']), $_POST['user_phone'], $_POST['account_status']);

 }

?>
          <?php 
            
            if(isset($_GET['customer_id'])) {

              $user = get_user_single(htmlspecialchars($_GET['customer_id']));              

            } 

          ?>

          <h1 class="page-header">Edit User</h1>

          <?php if(isset($GLOBALS['msg']['user_was_updated'])) { ?>

            <p class="bg-success">User Updated</p>

          <?php } ?>

          <div class="row">
            <div class="col-lg-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-user"></i> Update User Profile</h3>
                </div>
                <div class="panel-body">

                    <form method="post" action="">

                      <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo htmlspecialchars($user['first_name']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo htmlspecialchars($user['last_name']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="user_email">Email address</label>
                        <input type="email" class="form-control" id="user_email" name="user_email" value="<?php echo htmlspecialchars($user['user_email']); ?>" disabled>
                      </div>
                      <div class="form-group">
                        <label for="user_phone">Phone Number</label>
                        <input type="phone" class="form-control" id="user_phone" name="user_phone" value="<?php echo htmlspecialchars($user['user_phone']); ?>">
                      </div>
                      <div class="form-group">
                      <?php if($user['account_status'] == 'active'){ ?>
                        <label for="account_status">Account Status <i class="fa fa-check active"> Active</i></label>
                      <?php } else { ?>
                        <label for="account_status">Account Status <i class="fa fa-times suspended"> Suspended</i></label>
                      <?php } ?>
                        <div class="radio">
                          <label>
                            <input type="radio" id="account_status" name="account_status" value="active" <?php if($user['account_status'] == "active") echo "checked";?>>
                            Active
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" id="account_status" name="account_status" value="suspended" <?php if($user['account_status'] == "suspended") echo "checked";?>>
                            Suspended
                          </label>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <label for="exampleInputEmail1">New Password</label>
                        <input type="password" class="form-control" id="new_pass">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Confirm New Password</label>
                        <input type="password" class="form-control" id="confirm_pass">
                      </div> -->

                      <a href="#"><button type="submit" name="update_user" id="update_user" class="btn btn-primary">Update</button></a>

                    </form>

                </div><!-- end panel body -->
              </div><!-- end panel -->
            </div><!-- end col-6 -->

            <div class="col-lg-6">
              <div class="panel panel-default">

                <div class="panel-body">
                  <p class="bold">User Gravatar</p>

                  <img id="display-gravatar" src="<?php echo htmlspecialchars($user['user_gravatar']) . '?s=140&amp;d=mm&amp;r=g'; ?>">

                </div><!-- end panel-body -->
              
              </div><!-- end panel -->
            </div><!--end col-lg-6 -->
          </div><!-- end row -->

<?php include('dashboard-footer.php'); ?>