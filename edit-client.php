<?php 
/**
 * Edit User template 
 *
 * Used to display the edit profile page to the superuser account 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 can_user_view_page($_SESSION['userid']);


 if(isset($_POST['update_client'])){

    //check for sandbox checkbox
    if(isset($_POST['product_sandbox']) ? $product_sandbox = $_POST['product_sandbox'] : $product_sandbox = 0);

    //check for website checkbox
    if(isset($_POST['product_website']) ? $product_website = $_POST['product_website'] : $product_website = 0);

    //check for writing checkbox
    if(isset($_POST['product_writing']) ? $product_writing = $_POST['product_writing'] : $product_writing = 0);

    //check for video checkbox
    if(isset($_POST['product_video']) ? $product_video = $_POST['product_video'] : $product_video = 0);

    //check for bizdev checkbox
    if(isset($_POST['product_bizdev']) ? $product_bizdev = $_POST['product_bizdev'] : $product_bizdev = 0);

    //update entry
    update_client(htmlspecialchars($_GET['customer_id']), htmlspecialchars($_POST['client_company']), htmlspecialchars($_POST['primary_contact']), htmlspecialchars($_POST['primary_email']), htmlspecialchars($_POST['primary_phone']), htmlspecialchars($_POST['primary_cell']), htmlspecialchars($_POST['secondary_contact']), htmlspecialchars($_POST['secondary_email']), htmlspecialchars($_POST['client_address']), htmlspecialchars($_POST['client_city']), htmlspecialchars($_POST['client_state']), htmlspecialchars($_POST['client_zip']), htmlspecialchars($_POST['client_website']), htmlspecialchars($_POST['assigned_team']), $product_sandbox, $product_website, $product_writing, $product_video, $product_bizdev);

 }

?>
          <?php 
            
            if(isset($_GET['customer_id'])) {

              $client = get_client_single(htmlspecialchars($_GET['customer_id']));              

            } 

          ?>

          <h1 class="page-header">Edit Client</h1>

          <?php if(isset($GLOBALS['msg']['user_was_updated'])) { ?>

            <p class="bg-success">User Updated</p>

          <?php } ?>

          <div class="row">
            <div class="col-lg-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-user"></i> Edit Client Profile</h3>
                </div>
                <div class="panel-body">

                    <form method="post" action="">

                      <div class="form-group">
                        <label for="client_company">Company Name</label>
                        <input type="text" class="form-control" id="client_company" name="client_company" value="<?php echo htmlspecialchars($client['client_company']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="primary_contact">Primary Contact</label>
                        <input type="text" class="form-control" id="primary_contact" name="primary_contact" value="<?php echo htmlspecialchars($client['primary_contact']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_email">Primary Email</label>
                        <input type="email" class="form-control" id="primary_email" name="primary_email" value="<?php echo htmlspecialchars($client['primary_email']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_phone">Primary Phone</label>
                        <input type="phone" class="form-control" id="primary_phone" name="primary_phone" value="<?php echo htmlspecialchars($client['primary_phone']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="cell_phone">Primary Mobile</label>
                        <input type="phone" class="form-control" id="primary_cell" name="primary_cell" value="<?php echo htmlspecialchars($client['primary_phone']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="secondary_contact">Secondary Contact</label>
                        <input type="text" class="form-control" id="secondary_contact" name="secondary_contact" value="<?php echo htmlspecialchars($client['secondary_contact']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_email">Secondary Email</label>
                        <input type="email" class="form-control" id="secondary_email" name="secondary_email" value="<?php echo htmlspecialchars($client['secondary_email']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_address">Address</label>
                        <input type="text" class="form-control" id="client_address" name="client_address" value="<?php echo htmlspecialchars($client['client_address']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_city">City</label>
                        <input type="text" class="form-control" id="client_city" name="client_city" value="<?php echo htmlspecialchars($client['client_city']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_city">State</label>
                        <input type="text" class="form-control" id="client_state" name="client_state" value="<?php echo htmlspecialchars($client['client_state']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_city">Zip</label>
                        <input type="text" class="form-control" id="client_zip" name="client_zip" value="<?php echo htmlspecialchars($client['client_zip']); ?>">
                      </div>
                      <div class="form-group">
                        <label for="client_website">Website</label>
                        <input type="text" class="form-control" id="client_website" name="client_website" value="<?php echo htmlspecialchars($client['client_website']); ?>">
                      </div>
                      <div class="form-group form">
                       <p><strong>Team</strong></p>
                        <select name="assigned_team" id="assigned_team" class="form-control" style="width:30%">
                          <option></option>
                          <option value="string" <?php if (strtolower(htmlspecialchars($client['assigned_team'])) == 'string'){echo 'selected'; } ?>>String</option>
                          <option value="can" <?php if (strtolower(htmlspecialchars($client['assigned_team'])) == 'can'){echo 'selected'; } ?>>Can</option>
                        </select>
                      </div>
                      <p><strong>Products</strong></p>
                      <div class="form-group">
                        <label class="checkbox-inline">
                          <input type="checkbox" id="product_sandbox" name="product_sandbox" value="1" <?php if (htmlspecialchars($client['product_sandbox']) == '1'){echo 'checked'; } ?>> Sandbox
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="product_website" name="product_website" value="1" <?php if (htmlspecialchars($client['product_website']) == '1'){echo 'checked'; } ?>> Website
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="product_writing" name="product_writing" value="1" <?php if (htmlspecialchars($client['product_writing']) == '1'){echo 'checked'; } ?>> Writing
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="product_video" name="product_video" value="1" <?php if (htmlspecialchars($client['product_video']) == '1'){echo 'checked'; } ?>> Video
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" id="product_bizdev" name="product_bizdev" value="1" <?php if (htmlspecialchars($client['product_bizdev']) == '1'){echo 'checked'; } ?>> Business Development
                        </label>
                      </div>


                      <button type="submit" name="update_client" id="update_client" class="btn btn-primary">Update</button>

                    </form>

                </div><!-- end panel body -->
              </div><!-- end panel -->
            </div><!-- end col-6 -->

            <div class="col-lg-6">
              <div class="panel panel-default">

                <div class="panel-body">
                  <!-- <p class="bold">User Gravatar</p>

                  <img id="display-gravatar" src="<?php //echo htmlspecialchars($user['user_gravatar']) . '?s=140&amp;d=mm&amp;r=g'; ?>">-->
                  <img src="http://stringcan.com/wp-content/uploads/2015/02/SC_LOGO_LeadWithWhy_web-horizontal.png" alt="Stringcan | Lead With Why" id="logo">

                </div><!-- end panel-body -->
              
              </div><!-- end panel -->
            </div><!--end col-lg-6 -->
          </div><!-- end row -->

<?php include('dashboard-footer.php'); ?>