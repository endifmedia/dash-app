<?php 
/**
 * Wrike template 
 *
 * Used to display the wrike tasks page
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

?>

          <h1 class="page-header">Tasks in Wrike</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>         
                  <th>Task</th>
                  <th>Created</th>
                  <th>Due</th>
                  <th>Link</th>
                </tr>
              </thead>
              <tbody>

	             <?php 

		          	$response = get_wrike($_GET['code']); 

					//var_dump($response);
		          	$tasks = $response['data'];
		          	
		          	foreach($tasks as $task) {

		          		echo '<tr>';

		          			echo '<td>' . $task['title'] . '</td>';

		          			if(isset($task['dates']['start'])){ 

			          			echo '<td>' . date("M n, Y", strtotime($task['dates']['start'])) . '</td>';	

			          		} else {

			          			echo '<td>Not started yet</td>';
			          			
			          		}

			          		if(isset($task['dates']['due'])){ 

			          			echo '<td>' . date("M n, Y", strtotime($task['dates']['due'])) . '</td>';

			          		} else {

			          			echo '<td>No due date set</td>';

			          		}
		          			echo '<td><a href="' . $task['permalink'] . '" target="_blank"> View in Wrike</a></td>';

		          		echo '</tr>';

		          	}
          		?>

              </tbody>
            </table>
          </div>


<?php include('dashboard-footer.php'); ?>