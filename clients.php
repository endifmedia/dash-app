<?php 
/**
 * Users template 
 *
 * Used to display the users page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 can_user_view_page($_SESSION['userid']);
  
?>
       
          <h1 class="page-header">Clients <a href="client-new.php" class="btn btn-default pull-right"><i class="fa fa-plus"> Add New</i></a></h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Company</th>
                  <th>Primary Contact</th>
                  <th>Primary Email</th>
                  <th>Phone</th>
                  <th>Mobile</th>
                  <th>Secondary Contact</th> 
                  <th>Secondary Email</th>
                  <th>Team</th>
                  <th>Active</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              <?php 
              
                  $clients = get_clients();
                  //$n = 0;
                  $html = '';

                  foreach ($clients as $client) {

                    $html .= '<tr>';

                      //$html .= '<td><img src="' . htmlspecialchars($client['user_gravatar']) . '?s=35&d=mm&r=g"></td>';
                      //$html .= '<td>BLank</td>';
               
                      $html .= '<td>' . htmlspecialchars($client['client_company']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['primary_contact']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['primary_email']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['primary_phone']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['primary_cell']) . '</td>';//team
                      $html .= '<td>' . htmlspecialchars($client['secondary_contact']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['secondary_email']) . '</td>';
                      $html .= '<td>' . htmlspecialchars($client['assigned_team']) . '</td>';//assigned_team
                      
                      if($client['account_status'] == 'active'){

                        $html .= '<td><i class="fa fa-check active"></i></td>';

                      } else {

                        $html .= '<td><i class="fa fa-times suspended"></i>'; 

                      }
                      $html .= '<td>';
                        if(is_super_admin($_SESSION['userid'])){

                          $html .= '<a href="#" id="delete_user"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-times" title="delete"> Delete</i></button></a> ';
                        
                        }
                        $html .= '<a href="edit-client.php?customer_id=' . htmlspecialchars($client['ID']) . '" id="edit_user"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" title="edit"> Edit</i></button></a> '; 
                        $html .= '<a href="client-overview.php?customer_id=' . htmlspecialchars($client['ID']) . '" id="view_user"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-tachometer" title="delete"> View Dash</i></button>';
                      $html .= '</td>';

                    $html .= '</tr>'; 

                  }

                  echo $html; 

              ?>

              </tbody>
            </table>
          </div><!-- end table-responsive -->

<?php include('dashboard-footer.php'); ?>