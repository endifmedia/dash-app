<?php 
/**
 * Header template 
 *
 * Used to display the page header
 * @version 1.1 dashboard app
 */

  //include functions
  include('functions.php');

  session_start();

  user_logged_in();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title>DASH App</title>

    <!-- Bootstrap core CSS -->
    <link href="dash-bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="assets/fancybox/jquery.fancybox.css" rel="stylesheet">

    <!-- Easy Pie Chart -->
    <link rel="stylesheet"type="text/css" href="assets/css/jquery.easy-pie-chart.css">

    <!-- Chartist -->
    <link rel="stylesheet"type="text/css" href="assets/css/chartist.min.css">


    <!-- FormValidation CSS file -->
    <!-- <link rel="stylesheet" href="assets/formvalidation/dist/css/formValidation.min.css"> -->

    <!-- Font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Branding -->
          <a class="navbar-brand" href="index.php"><i class="fa fa-tachometer"></i> <span class="dash">DASH</span><span class="dot">.</span><span class="app">app</span></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

          <?php if(is_super_admin($_SESSION['userid'])){ ?>
            <li><a href="users.php"><i class="fa fa-coffee"></i> Stringcan Users </a></li>
          <?php } ?>

          <?php if(is_admin($_SESSION['userid'])){ ?>
            <li><a href="clients.php"><i class="fa fa-user"></i> Clients </a></li>
          <?php } ?>

          <?php if(is_admin($_SESSION['userid'])){ ?>
            <li><a href="settings.php"><i class="fa fa-cog"></i> Settings </a></li>
            <!-- <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear"></i> Settings <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="users.php"><i class="fa fa-coffee"></i> Stringcan Users </a></li>
                    <li><a href="clients.php"><i class="fa fa-user"></i> Clients </a></li>
                    <li class="divider"></li>
                    <li id="li_config"><a href="settings.php"><i class="fa fa-cog"></i> System Config </a></li>
                  </ul>
            </li> -->
          <?php } else { ?>
             <li><a href="settings.php"><i class="fa fa-cog"></i> Settings </a></li>
          <?php } ?>

            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo get_gravatar('small', $_SESSION['userid']); ?>"><b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="profile.php"><i class="fa fa-user"></i> Profile </a></li>
                <li class="divider"></li>
                <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out </a></li>
              </ul>
            </li>
            <!-- <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a></li> -->
            <li><a href="help.php"><i class="fa fa-info"></i> Help</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">

      <?php if(is_admin($_SESSION['userid'])) { ?>

        <div class="col-sm-12 main">

      <?php } else { ?>

        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li id="li_home"><a href="dashboard.php"><i class="fa fa-fw fa-bar-chart-o"></i> Overview</a></li>
            <li id="li_analytics"><a href="analytics.php"><i class="fa fa-line-chart"></i> Analytics</a></li>
            <li id="li_calls"><a href="sandbox.php?page=email"><!--<i class="fa fa-th-large"></i>--><img src="assets/img/sandbox-menu-icon.jpg" style="width: 1em;"> Sandbox</a></li>
            <!-- <li id="li_social"><a href="https://www.wrike.com/oauth2/authorize?client_id=PAjSivwS&response_type=code"><img src="assets/img/wrike-favicon.ico"> Wrike</a></li> -->
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

      <?php } ?>