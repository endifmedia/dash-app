<?php 
/**
 * Settings template 
 *
 * Used to display the settings page 
 * @version 1.1 invoice app
 */

 include('dashboard-header.php'); 

 can_user_view_page($_SESSION['userid']);

 if(isset($_POST['settings_update'])){

   update_user_config(htmlspecialchars($_SESSION['userid']), $_POST['google_id'], $_POST['sandbox_username'], $_POST['sandbox_pass']); //,$_POST['wrike_id']);

 }

 ?>
         
         <?php 
            
            if(isset($_SESSION['userid'])) {

              $user = get_user_config(htmlspecialchars($_SESSION['userid']));  

            }

          ?>

          <h1 class="page-header">System Configuration</h1>

          <?php if(isset($GLOBALS['msg']['user_was_updated'])) { ?>

            <p class="bg-success">Settings Updated</p>

          <?php } ?>

          <div class="row">
            <div class="col-sm-8">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-cog"></i> Settings</h3>
                </div>
                <div class="panel-body">
                  <form method="post" action="">

                    <div class="form-group">
                      <label for="google_id">Google Analytics ID</label>
                      <input type="text" class="form-control" name="google_id" id="google_id" value="<?php echo htmlspecialchars($user['google_id']); ?>" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="google_id">Sandbox Username</label>
                      <input type="text" class="form-control" name="sandbox_username" id="sandbox_username" value="<?php echo htmlspecialchars($user['sandbox_username']); ?>" placeholder="">
                    </div>
                    <div class="form-group">
                      <label for="google_id">Sandbox Password</label>
                      <input type="password" class="form-control" name="sandbox_pass" id="sandbox_pass" value="<?php echo htmlspecialchars($user['sandbox_pass']); ?>" placeholder="">
                    </div>
                    <!--<div class="form-group">
                      <label for="google_id">Wrike ID</label>
                      <input type="password" class="form-control" name="wrike_id" id="wrike_id" value="<?php echo htmlspecialchars($user['wrike_id']); ?>" placeholder="">
                    </div>-->

                    <button type="submit" name="settings_update" id="settings_update" class="btn btn-primary">Submit</button>

                  </form>
                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div><!-- end col-6 -->
            <div class="col-sm-4">
              <div class="panel panel-default">
                <div class="panel-body">
                  <img src="">
                  <div class="form-group">
                      <p class="help-block">Click <a href="" target="_blank">here</a> to setup your google client id.</p>
                  </div>
                </div><!-- end panel-body -->
              </div><!--end panel -->
            </div><!-- end col-5 -->
          </div><!-- end row -->

<?php include('dashboard-footer.php'); ?>
