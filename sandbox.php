<?php 
/**
 * Sandbox template 
 *
 * Used to display the sandbox overview page.
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 if(isset($_GET['page'])){
	$page = $_GET['page'];
 }

?>
          
        <h1 class="page-header">Sandbox - <?php echo htmlspecialchars(ucfirst($page)); ?>  
	        <div class="pull-right">
	        	<a class="btn btn-default <?php echo $page == 'email' ? 'active' : ''; ?>" href="sandbox.php?page=email" role="button">Website Analytics/a>
			    <a class="btn btn-default <?php echo $page == 'email' ? 'active' : ''; ?>" href="sandbox.php?page=email" role="button">Email</a>
			    <a class="btn btn-default <?php echo $page == 'contacts' ? 'active' : ''; ?>" href="sandbox.php?page=contacts" role="button">Top Lead Scores</a>
				<!-- <button class="btn btn-default" type="submit">Overview</button> -->
			</div>
		</h1>

		<?php 

		if($page == 'email') { ?>
        
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>     
                  <th>Subject</th> 
                  <th># of recipients</th>   
                  <th># Of clicks</th>
                  <th># Of reads</th>
                  <th>Date Sent</th>
                </tr>
              </thead>
              <tbody>

	             <?php 

		          	$response = get_sandbox('email'); 

		          	foreach($response->Broadcasts->Broadcast as $sandbox) {

		          		echo '<tr>';

		          			//echo '<td>' . $sandbox->Status . '</td>';
		          			echo '<td>' . $sandbox->Subject . '</td>';
		          			echo '<td>' . $sandbox->NumberOfRecipients. '</td>';
		          			echo '<td>' . $sandbox->NumberOfClicks . '</td>';
		          			echo '<td>' . $sandbox->NumberOfReads . '</td>';
							echo '<td>' . date("M n, Y", strtotime($sandbox->DatetimeSent)) . '</td>';		          			

		          		echo '</tr>';

		          	}
          		?>

              </tbody>
            </table>
          </div>

          <?php } ?>

          <?php 

		if($page == 'contacts') { ?>
        
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>     
                  <th>Lead Name</th> 
                  <th>Phone</th>   
                  <th>Email</th>
                  <th>Activity Leadscore</th>
                </tr>
              </thead>
              <tbody>

	             <?php 

		          	$response = get_sandbox('leads'); 

		          	//var_dump($response);

		          	foreach($response->Contacts->Contact as $lead) {

			          	if($lead->ActivityLeadScore > 1 && $lead->Firstname !== '') {

			          		//var_dump($lead);

			          		echo '<tr>';

			          			//echo '<td>' . $sandbox->Status . '</td>';
			          			echo '<td>' . $lead->Firstname . ' ' . $lead->Lastname . '</td>';
			          			echo '<td>' . $lead->Phone. '</td>';
			          			echo '<td>' . $lead->Email . '</td>';
			          			//echo '<td>' . $lead->DemographicLeadScore . '</td>';
			          			echo '<td>' . $lead->ActivityLeadScore . '</td>';

			          		echo '</tr>';
			          	}

		          	}
          		?>

              </tbody>
            </table>
          </div>

          <?php } ?>

<?php include('dashboard-footer.php'); ?>