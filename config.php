<?php
/**
 * Configuration File. Default settings needed for the application to run.
 *
 * @package dashboard app
 */

/** Base directory **/
define('APP_BASE', dirname(__FILE__) . '/');

/** Assets directory **/
define('ASSETS', APP_BASE . 'assets/');

/** MySQL hostname */
define('DB_TABLE', 'dashboard_app');

 /**
  * Update this section with your database information
  *
  * @version 1.1
  */
  /** MySQL database username */
  define('DB_USER', 'root');

  /** MySQL database password */
  define('DB_PASS', 'root');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');

  /** App URL **/
  define('APP_URL', "http://localhost:8888/dash-app/");