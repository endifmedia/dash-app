<?php 
/**
 * Analytics template 
 *
 * Used to display the analytics page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 $client = get_client_single(htmlspecialchars($_GET['customer_id']));

?>

          <h1 class="page-header"><?php $client_company?> Client - <?php echo htmlspecialchars($client['client_company']); ?><a href="edit-client.php?customer_id=<?php echo htmlspecialchars($_GET['customer_id']); ?>" class="btn btn-default pull-right"><i class="fa fa-plus"> Edit</i></a></h1>

          <div class="row">
            <div class="col-sm-12 client-dropdown">
              <div class="input-group">
                <div class="input-group-btn">
                  
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Clients
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>

                  <ul class="dropdown-menu" role="menu">
                    <?php 
                      $items = get_client_list();
                      $html = '';

                      foreach($items as $item){
                        $html .=  '<li><a href="client-overview.php?customer_id='. $item['ID'] .'">';
                          if(strtolower($item['assigned_team']) == 'string'){
                            $html .= '<i class="fa fa-futbol-o"></i>'; 
                          }
                        $html .= $client['client_company'] . ' <i class="fa fa-linkedin-square fa-1"></i> <i class="fa fa-whatsapp"></i> <i class="fa fa-picture-o"></i> <i class="fa fa-phone-square"></i></a></li>';
                      }
                      echo $html;
                    ?>
                    
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
            
              </div>
            </div>

            <div class="col-sm-8">

              <div class="panel panel-default">
                <div class="panel-body">

                  <p class="title">Reports </p>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <div class="box" data-percent="73" style="background-color: rgb(240, 91, 79);color: white;padding: 37px 10px;text-align: center;">
                
                            <div>
                              <span style="font-size: 3em;font-weight: 100;"><?php alexa_rank($_GET['customer_id']); ?></span><br>
                              <span>alexa rank</span> 
                            </div>

                          </div>
                          
                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">

                          <i class="fa fa-envelope-o fa-6"></i>

                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">

                          <i class="fa fa-bar-chart fa-6"></i>

                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">

                          <i class="fa fa-beer fa-6"></i>

                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">

                          <i class="fa fa-pie-chart fa-6"></i>

                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                    <div class="col-sm-4">
                      <div class="panel panel-default">
                        <div class="panel-body">

                          <i class="fa fa-pie-chart fa-6"></i>

                        </div><!-- end panel-body -->
                      </div><!-- end panel -->
                    </div>
                  </div>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

            <div class="col-sm-4">

              <div class="row">
                <p class="title">Saved Searches <span><a href="user-new.php" class=""><i class="fa fa-plus"> Add New</i></a></span></p>
              </div>
              <div class="row">
                <span>Mar 23, 2015 - <a href="saved-searches.php?saved_search=2">Ice Cream Vendors</a> - 300 entries</span>
              </div>

            </div>

          </div>

<?php include('dashboard-footer.php'); ?>