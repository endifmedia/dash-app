<?php
/**
 * Delete Script
 *
 * Used to remove single items from from the database
 * @version 1.1 dashboard app 
 */	

  include_once('../config.php');

  //grab id to remove
  $id = $_POST['id'];

  //grab table to remove from
  $table = $_POST['table'];

  //connect to database
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_TABLE); 
    
  //try deleting record using the record ID we received from POST
  $delete_row = $mysqli->query("DELETE FROM " . $table . " WHERE id=" . $id);
    
  $mysqli->close(); //close db connection

?>