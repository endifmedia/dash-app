<?php 
/**
 * Analytics template 
 *
 * Used to display the analytics page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 //get_user_single($_GET['search_id']);

?>

          <h1 class="page-header"><?php $Saved_Search_Name?> Saved Search - Ice Cream Vendors</h1>

          <div class="row">

            <div class="col-sm-9">

              <div class="panel panel-default">
                <div class="panel-body">

                  <p class="title">Search Criteria </p>

                  <form method="post" action="">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Search Title</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Name this search">
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="invoice_product_3">Company</label>
                            <select name="invoice_product_3" id="invoice_product_3" class="form-control invoice-product">
                              <option></option>
                              <option value="Hourly Rate" title="25.00">Hourly Rate</option>
                              <option value="Invoice App websoftware" title="15.00">Invoice App websoftware</option>
                              <option value="WAAM Site (first project)" title="850">WAAM Site (first project)</option>                            
                            </select>
                          </div>
                      </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="invoice_product_price_3">Client Name</label>
                            <select name="invoice_product_3" id="invoice_product_3" class="form-control invoice-product">
                              <option></option>
                              <option value="Hourly Rate" title="25.00">Hourly Rate</option>
                              <option value="Invoice App websoftware" title="15.00">Invoice App websoftware</option>
                              <option value="WAAM Site (first project)" title="850">WAAM Site (first project)</option>                            
                            </select>                          
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="invoice_product_3">City</label>
                            <select name="invoice_product_3" id="invoice_product_3" class="form-control invoice-product">
                              <option></option>
                              <option value="Hourly Rate" title="25.00">Hourly Rate</option>
                              <option value="Invoice App websoftware" title="15.00">Invoice App websoftware</option>
                              <option value="WAAM Site (first project)" title="850">WAAM Site (first project)</option>                            
                            </select>
                          </div>
                      </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="invoice_product_price_3">State</label>
                            <select name="invoice_product_3" id="invoice_product_3" class="form-control invoice-product">
                              <option></option>
                              <option value="Hourly Rate" title="25.00">Hourly Rate</option>
                              <option value="Invoice App websoftware" title="15.00">Invoice App websoftware</option>
                              <option value="WAAM Site (first project)" title="850">WAAM Site (first project)</option>                            
                            </select>                          </div>
                        </div>
                    </div>
                    <button type="submit" name="save_search" id="save_search" class="btn btn-default">Save</button>
                  </form>

                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div>

            <div class="col-sm-3">

              <div class="row">
                <p class="title">Export this List</p>
              </div>
              <div class="row">
              <span><a href="export-csv.php?search_id=63" id="export_search"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-reply-all"></i> Export as .CSV</i></button></a></span>
              </div>

            </div>

          </div>

<?php include('dashboard-footer.php'); ?>