<?php 
/**
 * Users template 
 *
 * Used to display the add new users page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 

 superuser_only($_SESSION['userid']);

?>
         
          <h1 class="page-header">Users</h1>

          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-user"></i> Create a New User</h3>
                </div>
                <div class="panel-body">
                  <form>
                    <div class="form-group">
                      <label for="">First Name</label>
                      <input type="text" class="form-control" id="address" placeholder="Enter address">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Last Name</label>
                      <input type="text" class="form-control" id="zip" placeholder="Enter zip">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" class="form-control" id="emailaddress" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Phone Number</label>
                      <input type="number" class="form-control" id="website" placeholder="Enter website">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Username</label>
                      <input type="text" class="form-control" id="companyname" placeholder="Enter company name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Password</label>
                      <input type="password" class="form-control" id="companyname" placeholder="Enter company name">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Confirm Password</label>
                      <input type="password" class="form-control" id="companyname" placeholder="Enter company name">
                    </div>

                    <input type="hidden" id="gravatar" value="">

                    <a href=""><button type="submit" class="btn btn-primary addnew-submit">Submit</button></a>
                    
                  </form>
                </div><!-- end panel-body -->
              </div><!-- end panel -->
            </div><!-- end col-6 -->
            <div class="col-sm-6">
              <div class="panel panel-default">
                <div class="panel-body">
                  <img src="">
                  <div class="form-group">
                    <p class="bold">Your Gravatar</p>
                    <img id="display-gravatar" src="">
                    <div class="display-gravatar-text">
                      <p class="help-block">A "gavatar" is an image that represents you online — a little picture that appears next to your name when you interact with websites.</p>
                      <p class="help-block">If you want to upload your own image <a href="https://en.gravatar.com/" target="_blank">create your profile</a> just once, and then when you participate in any Gravatar-enabled site, your Gravatar image will automatically follow you there.</p>
                    </div>
                  </div>
                </div><!-- end panel-body -->
              </div><!--end panel -->
            </div><!-- end col-5 -->
          </div><!-- end row -->

<?php include('dashboard-footer.php'); ?>
