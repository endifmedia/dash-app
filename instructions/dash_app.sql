-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2015 at 01:43 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `invoice_app`
--
-- CREATE DATABASE IF NOT EXISTS `dash_app`;

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(64) NOT NULL DEFAULT '',
  `user_email` varchar(255) DEFAULT NULL,
  `user_phone` varchar(22) DEFAULT NULL,
  `access_level` int(11) NOT NULL DEFAULT '1',
  `google_id` varchar(255) DEFAULT NULL,
  `user_website` varchar(100) NOT NULL DEFAULT '',
  `account_status` char(10) NOT NULL DEFAULT 'active',
  `twilio_id` varchar(255) DEFAULT NULL,
  `twilio_token` varchar(255) DEFAULT NULL,  
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

CREATE TABLE IF NOT EXISTS `clients` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_company` varchar(255) DEFAULT NULL,
  `primary_contact` varchar(255) DEFAULT NULL,
  `secondary_contact` varchar(255) DEFAULT NULL,
  `client_email` varchar(255) DEFAULT NULL,
  `client_address` varchar(64) NOT NULL DEFAULT '',
  `client_city` varchar(64) NOT NULL DEFAULT '',
  `client_state` varchar(64) NOT NULL DEFAULT '',
  `client_zip` char(6) DEFAULT NULL,
  `client_phone` varchar(22) DEFAULT NULL,
  `cell_phone` varchar(22) DEFAULT NULL,
  `client_website` varchar(100) NOT NULL DEFAULT '',
  `google_id` varchar(255) DEFAULT NULL,
  `twilio_id` varchar(255) DEFAULT NULL,
  `twilio_token` varchar(255) DEFAULT NULL, 
  `sandbox_id` varchar(255) DEFAULT NULL, 
  `sandbox_apikey` varchar(255) DEFAULT NULL, 
  `user_password` varchar(64) NOT NULL DEFAULT '',
  `access_level` int(11) NOT NULL DEFAULT '2', 
  `account_status` char(10) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;











