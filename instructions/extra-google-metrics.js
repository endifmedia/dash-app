dimensions=ga:mobileDeviceInfo,ga:source
metrics: ga:sessions,ga:pageviews,ga:sessionDuration'
segment=gaid::-14

/* keyword */
dimensions: 'ga:keyword',
metrics: 'ga:sessions',

/* mobile devices */
dimensions: 'ga:mobileDeviceInfo',
metrics: 'ga:sessions',

/* top referals */
dimensions: 'ga:source',
metrics: 'ga:pageviews',

/* top exit pages */
dimensions=ga:exitPagePath
metrics=ga:exits,ga:pageviews
sort=-ga:exits

/* top viewed pages */ 
dimensions: 'ga:pagePath',
metrics: 'ga:pageviews,ga:uniquePageviews,ga:timeOnPage,ga:bounces,ga:entrances,ga:exits',

/* top landing pages */
dimensions=ga:landingPagePath
metrics=ga:entrances,ga:bounces
sort=-ga:entrances











