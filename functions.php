<?php
/**
 * Model file for the invoice.app application. All of the extra functionality 
 * is found in this file. 
 *
 * @package dash.app
 */

include('config.php');


/**
 * var strMD5 = $.md5(strVal);
 * var user = jQuery(on unfocus of email input).val().stringtolower().md5();
 * jQuery(user).stringtolower().md5();
 */


/* RETURN GOOGLE ID

<script type="text/javascript" charset="utf-8">

function get_UA() {

  txt = document.getElementById('scripttag').value;

  var matches = txt.match(/(UA-[\d-]+)/);

  if (matches[1]) {

    alert(matches[1]);

  }

}

</script>

<textarea name="script" cols="40" id="scripttag" rows="10"></textarea>

<button onclick="javascript:get_UA();">get account id</button>

*/


/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source http://gravatar.com/site/implement/images/php/
 */
/*function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'http://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}*/

/**
 * Checks if user is registered with the site.
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
 /*function can_user_view_page($id){

  require(APP_BASE . "/assets/database.php");

  try {

    $results = $db->prepare("SELECT access_level FROM users WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $level = $results->fetch(PDO::FETCH_ASSOC);

  if($level['access_level'] != 1){

    echo 'You do not have permission to access this page';
    exit();

  }


 }*/

 function alexa_rank($id){

  require(APP_BASE . "/assets/database.php");

  try {

    $results = $db->prepare("SELECT client_website FROM clients WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $url = $results->fetch(PDO::FETCH_ASSOC);

  $xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$url['client_website']);

  //var_dump($xml);

  $rank = isset($xml->SD[1]->POPULARITY)?$xml->SD[1]->POPULARITY->attributes()->TEXT:0;
  $web = (string)$xml->SD[0]->attributes()->HOST;

  //echo 'Your website ' . $web . " has an Alexa Rank of " . $rank;

  //echo 'Alexa rank ' . $rank;
  echo $rank;
            
}

/**
 * Retrieves google analytics information for each client.
 *
 * use: call <?php access_google(); ?>
 *
 * @version 1.1
 */
function access_google(){

  include('assets/GoogleAnalyticsAPI.class.php');

  $ga = new GoogleAnalyticsAPI(); 
  $ga->auth->setClientId('294885830237-rk0polionkb9nnlf772vpeklpeeab8ml.apps.googleusercontent.com'); // From the APIs console
  $ga->auth->setClientSecret('jDSj2XV0l2lP_q3BukazI55e'); // From the APIs console
  $ga->auth->setRedirectUri('http://dash.getstringcan.com'); // Url to your app, must match one in the APIs console

  // Get the Auth-Url

  if (isset($_GET['force_oauth'])) {
        $_SESSION['oauth_access_token'] = null;
  }

  if (!isset($_SESSION['oauth_access_token']) && !isset($_GET['code'])) {
        // Go get the url of the authentication page, redirect the client and go get that token!
        $url = $ga->auth->buildAuthUrl();

        echo '<a href="' . $ga->auth->buildAuthUrl() . '" class="google-popup fancybox.iframe" style="display: inline-block;background: #f7991c;color: #fff;width: 200px;border-radius: 2px;white-space: nowrap;padding: 6px 10px;"><i class="fa fa-line-chart"></i> View My Analytics</a>';
        //exit();
  }

  if (!isset($_SESSION['oauth_access_token']) && isset($_GET['code'])) {
        $auth = $ga->auth->getAccessToken($_GET['code']);
        if ($auth['http_code'] == 200) {
            $accessToken    = $auth['access_token'];
            $refreshToken   = $auth['refresh_token'];
            $tokenExpires   = $auth['expires_in'];
            $tokenCreated   = time();
            
            // For simplicity of the example we only store the accessToken
            // If it expires use the refreshToken to get a fresh one
            $_SESSION['oauth_access_token'] = $accessToken;

           // do_google();

        } else {
            die("Sorry, something wend wrong retrieving the oAuth tokens");
        }
  } 

  if(isset($_SESSION['oauth_access_token'])){

    $ga->setAccessToken($_SESSION['oauth_access_token']);
    $ga->setAccountId('ga:xxxxxxx');

    // Load profiles
    $profiles = $ga->getProfiles();
    $accounts = array();
    foreach ($profiles['items'] as $item) {
        $id = "ga:{$item['id']}";
        $name = $item['name'];
        $accounts[$id] = $name;
    }
    // Print out the Accounts with Id => Name. Save the Id (array-key) of the account you want to query data. 
    // See next chapter how to set the account-id.
    //var_dump($accounts);

    $ga->setAccessToken($_SESSION['oauth_access_token']);
    $ga->setAccountId('ga:101285609');

    // Set the default params. For example the start/end dates and max-results
    $defaults = array(
        'start-date' => date('Y-m-d', strtotime('-1 month')),
        'end-date' => date('Y-m-d'),
    );
    $ga->setDefaultQueryParams($defaults);

    // Example1: Get visits by date
    $params = array(
        'metrics' => 'ga:visits',
        'dimensions' => 'ga:date',
    );
    $visits = $ga->query($params);

    // Example2: Get visits by country
    $params = array(
        'metrics' => 'ga:visits',
        'dimensions' => 'ga:country',
        'sort' => '-ga:visits',
        'max-results' => 30,
        'start-date' => '2013-01-01' //Overwrite this from the defaultQueryParams
    ); 
    $visitsByCountry = $ga->query($params);

    // Example3: Same data as Example1 but with the built in method:
    $visits = $ga->getVisitsByDate();

    // Example4: Get visits by Operating Systems and return max. 100 results
    $visitsByOs = $ga->getVisitsBySystemOs(array('max-results' => 100));

    // Example5: Get referral traffic
    $referralTraffic = $ga->getReferralTraffic();

    // Example6: Get visits by languages
    $visitsByLanguages = $ga->getVisitsByLanguages();

    $audienceStats = $ga->getAudienceStatistics();

    $lastMonthAudienceStats = $ga->getAudienceStatistics( array(                                                             
                                                            'start-date' => date('Y-m-d', strtotime('-2 month')),
                                                            'end-date' => date('Y-m-d', strtotime('-1 month'))
                                                          ));

    //var_dump($visits);
    //echo '<div class=""><h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;float: left;margin-right: .5em;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Total Visits this Month </span>' . $visitsByLanguages['totalsForAllResults']['ga:visits'] . '</h1>';
    //echo '<h1>' . $referralTraffic['rows'][0][0] . ' ' . $referralTraffic['rows'][0][1] . '</h1>';
    //var_dump($referralTraffic['rows'][0][0]);

    echo '<div class="col-sm-4">

              <div class="box" data-percent="73" style="background-color: rgb(247, 153, 28);color: white;padding: 10px;">
                  <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-users"></i>
                </div>
                <div class="pull-right" style="width: 50%;  margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;width: 100%;">' . $lastMonthAudienceStats['totalsForAllResults']['ga:visits'] . '</span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">visits (last month)</span> 
                </div>      
              </div>

            </div>';

    echo '<div class="col-sm-4">

              <div class="box" data-percent="73" style="background-color: rgb(247, 153, 28);color: white;padding: 10px;">
                  <div class="" style="/* margin-top: -1.6em; */font-size: 4em; width: 25%;/* float: left; */">
                  <i class="fa fa-users"></i>
                </div>
                <div class="pull-right" style="width: 50%;  margin-top: -7em;">
                  <span style="font-size: 4em;font-weight: 100;text-align: right;float: right;width: 100%;">' . $audienceStats['totalsForAllResults']['ga:visits'] . '</span><br>
                  <span style="/* padding-left: 1em; */float: right;/* bottom: 0px; *//* right: 0px; *//* margin-top: 4em; */text-align: right;float: right;">visits ( month)</span> 
                </div>      
              </div>

            </div>';

   
    //echo '<h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Total Visits this Month </span>' . $audienceStats['totalsForAllResults']['ga:visitors'] . '</span></h1>';
    echo '<h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;float: left;margin-right: .5em;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Percent New Visits </span>' . number_format($audienceStats['totalsForAllResults']['ga:percentNewVisits'], 2, '.', '') . '%</span></h1>';
    echo '<h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;float: left;margin-right: .5em;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Bounce Rate </span>' . number_format($audienceStats['totalsForAllResults']['ga:visitBounceRate'], 2, '.', '') . '%</span></h1>';//number_format($number, 2, '.', '')
    //echo '<h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Average Time On Site </span>' . $audienceStats['totalsForAllResults']['ga:avgTimeOnSite'] . '</span></h1>';
    echo '<h1 style="background-color: orange;width: 25%;text-align: center;padding: 8px 7px;float: left;margin-right: .5em;"><span style="font-weight: 100;font-size: 0.8em;text-align: center;">Direct Referrals </span>' . $referralTraffic['rows'][0][1] . '</h1></div>';

    //var_dump($audienceStats);

  }

}


/**
 * Used by Administrators to update existing user profiles
 *
 * use: call <?php admin_update_user($id, $user_phone, $status) ?>
 *
 * @param   $id            The user ID
 * @param   $user_phone    The user phone 
 * @param   $status        The user status
 * @version 1.1
 */
 //function admin_update_user($id, $first_name, $last_name, $user_email, $user_phone, $status ){
function admin_update_user($id, $user_phone, $status){

  require(APP_BASE . "/assets/database.php");

  //update statement
  $sql = "UPDATE `users` SET `user_phone` = :phone, `account_status` = :status WHERE id = :id";

  try {

    $update = $db->prepare($sql);
    $update->bindParam(':phone', $user_phone,PDO::PARAM_STR);
    $update->bindValue(':status', $status, PDO::PARAM_STR);
    $update->bindValue(':id', $id, PDO::PARAM_STR);
    $update->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $GLOBALS['msg']['user_was_updated'] = true;  

}

/**
 * Checks if user is registered with the site.
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function get_gravatar($size = '', $id){

  require(APP_BASE . "/assets/database.php");

  try {

    $results = $db->prepare("SELECT user_gravatar FROM users WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $gravatar = $results->fetch(PDO::FETCH_ASSOC);

  //pr($gravatar['user_gravatar']);

  if($gravatar['user_gravatar'] != ''){

    if($size == 'big'){
      return $gravatar['user_gravatar'] . '?s=140&d=mm&r=g';
      exit();
    }

    if($size == 'small'){
      return $gravatar['user_gravatar'] . '?s=20&d=mm&r=g';
      exit();
    }

  } else {

    //print('<i class="fa fa-user-secret"></i>');

  }

}

/**
 * Checks if user is registered with the site.
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function verify_user($user_email, $password) { 

  if($user_email && $password){

    //if(isset($login) && )
    require(APP_BASE . "/assets/database.php");

    $password = md5($password); // . 'D@$h/appEndIFM3diA'


    try{

      $results = $db->prepare("SELECT ID, user_email, user_password, account_status FROM users WHERE user_email = :email AND user_password = :password LIMIT 1");
      $results->bindParam(':email',$user_email);
      $results->bindParam(':password',$password);
      $results->execute();

    } catch(Exception $e) {

      return false;
      exit();

    }

    $pass = $results->fetch(PDO::FETCH_ASSOC);

    if ($password == $pass['user_password']) {

      if($pass['account_status'] == 'active'){

        session_start();
         
        $_SESSION['timeout'] = time();
        $_SESSION['userid'] = $pass['ID'];
        $_SESSION['loggedin'] = 1;

        header('Location: ' . APP_URL . 'dashboard.php' );
        exit();

      } else {

       $GLOBALS['msg']['login-err'] = 'Your account has been suspended';
       $GLOBALS['msg']['class'] = ' bg-info';

      }


    } else {

      //return false;

      $GLOBALS['error']['class'] = ' animated shake';
      $GLOBALS['msg']['login-err'] = 'Login is invalid. Please try again';
      $GLOBALS['msg']['class'] = ' bg-danger';


    }

  } else {

    exit();

  }

}

/**
 * Returns user's Google Analutics API code
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function get_analytics_id($id){

  require(APP_BASE . "/assets/database.php");

  try{

    $results = $db->prepare("SELECT google_id FROM users WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

    //echo "Soo sorry, you suck and so does your code."

  } catch(Exception $e) {
    echo "Data could not be found.";
    exit();
  }

  $google_id = $results->fetch(PDO::FETCH_ASSOC);

  if($google_id['google_id']){

    print(htmlspecialchars($google_id['google_id']));

  } else {

    return false;

  }

}

/**
 * Checks if user is registered with the site.
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
 function can_user_view_page($id){

  require(APP_BASE . "/assets/database.php");

  try {

    $results = $db->prepare("SELECT access_level FROM users WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $level = $results->fetch(PDO::FETCH_ASSOC);

  if($level['access_level'] === '0'){

    echo 'You do not have permission to access this page';
    exit();

  }


 }

 /**
 * Checks if user is registered with the site.
 *
 * use: call <?php get_analytics_id(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
 function superuser_only($id){

  require(APP_BASE . "/assets/database.php");

  try {

    $results = $db->prepare("SELECT access_level FROM users WHERE ID = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {

    return false;
    exit();

  }

  $level = $results->fetch(PDO::FETCH_ASSOC);

  if($level['access_level'] != '2'){

    echo 'You do not have permission to access this page';
    exit();

  }


 }

/**
 * Creates userpassword 
 *
 * use: call <?php user_logged_in(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function add_user($username, $pass){
//
}

/**
 * Checks if user has access to certain content
 *
 * use: call <?php is_admin(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function is_admin($id){

  //1 == admin access 
  //0 == standard viewing access
  require(APP_BASE . "/assets/database.php");

  try{

    $results = $db->prepare("SELECT access_level FROM users WHERE id = :id");
    $results->bindParam(':id', $id);
    $results->execute();

    //echo "Soo sorry, you suck and so does your code."

  } catch(Exception $e) {
    echo "Data could not be found.";
    exit();
  }

  $access = $results->fetch(PDO::FETCH_ASSOC);

  if($access['access_level'] !='0' ){

    return true;

  } else {

    return false;

  }

}

/**
 * Checks if user is super admin
 *
 * use: call <?php is_super_admin(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function is_super_admin($id){
  //2 == super admin (users list - create/edit users) 
  //1 == admin access 
  //0 == standard viewing access
  require(APP_BASE . "/assets/database.php");

  try{

    $results = $db->prepare("SELECT access_level FROM users WHERE id = :id");
    $results->bindParam(':id', $id);
    $results->execute();

  } catch(Exception $e) {
    echo "Data could not be found.";
    exit();
  }

  $access = $results->fetch(PDO::FETCH_ASSOC);

  if($access['access_level'] === '2'){

    return true;

  } else {

    return false;

  }

}

/**
 * Checks if user is currently logged in
 *
 * use: call <?php user_logged_in(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function log_out(){

  if(!isset($_SESSION)){
    session_start();
  }

  session_destroy();
  header('Location: ' . APP_URL . 'login.php' );

}
/**
 * Checks if user is currently logged in
 *
 * use: call <?php user_logged_in(); ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function user_logged_in(){

  // set time-out period (in seconds)
  $inactive = 1800;
     
  // check to see if $_SESSION["timeout"] is set
  if (isset($_SESSION['timeout'])) {
      // calculate the session's "time to live"
      $sessionTTL = time() - $_SESSION["timeout"];
      if ($sessionTTL > $inactive) {
          session_destroy();
          header('Location: ' . APP_URL . 'login.php' );
      }

      $_SESSION["timeout"] = time();
  }

  //if $_SESSION doesnt exist or $_SESSION['loggedin'] does not exist or $_SESSION['loggedin'] does not equal 1 - redirect
  if(!isset($_SESSION) || !isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != '1'){

    header('Location: ' . APP_URL . 'login.php' );
    exit();

  }

  if (isset($_SESSION['HTTP_USER_AGENT'])) {

    if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT'])){

      header('Location: ' . APP_URL . 'login.php' );
      exit(); 

    } else {

      $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);

    }

  }

}

function get_wrike($code){

  function get_tasks($token){

    // (/api/v3/tasks?status=Active

    // /api/v3/folders/$client_name/tasks?status=Active
    $url = "https://www.wrike.com/api/v3/tasks?access_token=$token";
    //$url = "https://www.wrike.com/api/v3/folders?access_token=$token";
    //$url = "https://www.wrike.com/api/v3/folders/IEAAIIKJI4BKHBCY/tasks?access_token=$token";

    //$postdata = "client_id=$client&client_secret=$secret&grant_type=authorization_code&code=$code";

    $curl = curl_init( $url );

    //curl_setopt( $curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

    $data = curl_exec($curl);

    $response = json_decode($data, true);

    return $response['data'];

    curl_close($curl);

  }

  //click wrike link 
    //check if connection is live
      //if old - refresh

      //if dead - create new
        //re-direct to wrike page
        //run second function


  //get tasks($access token)
    //
  //if(1 != 0){
    $client = 'PAjSivwS';
    $secret = 'qTHW2bTtnVdc4jejW2t7kZhRevaRHaxDsanYxiOFe3P04lGFJGLYFg4nVPLRVBIF';

    $url = 'https://www.wrike.com/oauth2/authorize?client_id=EU1BMKkY&response_type=code';

    $url = 'https://www.wrike.com/oauth2/token';

    //$code = $_GET['code'];

    $postdata = "client_id=$client&client_secret=$secret&grant_type=authorization_code&code=$code";

    /*
    client_id=PAjSivwS
    client_secret=yw3lCQPOT2SBWkrWOfiICT5xKWyb8WXBgi1OamLVYT4kkit7rYRqBPugCqpIgzkw
    grant_type=authorization_code
    code=YR5PrHlcFEctMCwLUWJzncLYkpn1UcbuCOzr5vHe47gBxNz56cyT5LIdeLLyT3TQ-N
    */

    $curl = curl_init( $url );

    curl_setopt( $curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

    $data = curl_exec($curl);

    $response = json_decode($data, true);

    curl_close($curl);

    //get_tasks($response['access_token']);

    $code = $response['access_token'];
    // (/api/v3/tasks?status=Active

    // /api/v3/folders/$client_name/tasks?status=Active
    $url = "https://www.wrike.com/api/v3/tasks?access_token=$code";
    //$url = "https://www.wrike.com/api/v3/folders?access_token=$token";
    //$url = "https://www.wrike.com/api/v3/folders/IEAAIIKJI4BKHBCY/tasks?access_token=$code";

    //$postdata = "client_id=$client&client_secret=$secret&grant_type=authorization_code&code=$code";

    $curl = curl_init( $url );

    //curl_setopt( $curl, CURLOPT_POSTFIELDS, $postdata);

    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);

    $data = curl_exec($curl);

    $response = json_decode($data, true);

    return $response;

    //curl_close($curl);

    //var_dump($response);



  

  //http://localhost/?code=YR5PrHlcFEctMCwLUWJzncLYkpn1UcbuCOzr5vHe47gBxNz56cyT5LIdeLLyT3TQ-N

  //$url = 'https://www.wrike.com/oauth2/authorize?client_id=EU1BMKkY&response_type=code';

  //start connection
  //$curl =  curl_init('http://wrike.api.awesome');

  //set user and password
  //curl_setopt($curl, CURLOPT_USERPWD, 'username:password');

  //tell cURL to fail if an erro occurs
  //curl_setopt($curl, CURLOPT_FAILONERROR, 1);

  //allow for redirects
  //curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

  //assign response to variable
  //curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

  //set timeout
  //curl_setopt($curl, CURLOPT_TIMEOUT, 5);

  //use POST
  //curl_setopt($curl, CURLOPT_POST, $post_info);

  //set POST data
  //curl_setopt($curl, CURLOPT_POSTFIELDS, 'name=foo&pass=bar&format=csv');

  //execute
  //$r = curl_exec($curl);

  //close the connection
  //curl_close($curl);

  //print results
  //print($r);
}

function get_sandbox($activity){

  // What are we testing?
  function Send_CE_XML_Request($request) {
      $url = "https://api.stgi.net/xml.pl";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);

      // turning off the server and peer verification(TrustManager Concept).
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
      
      return curl_exec($ch);
  }

  $user = get_user_config($_SESSION['userid']);

  //
  $API_UserName = urlencode($user['sandbox_username']);
  //
  $API_Password = urlencode($user['sandbox_pass']);

  $API_XML = urlencode("<GetAuthTokenRequest>\n</GetAuthTokenRequest>\n");

  // NVPRequest for submitting to server
  $nvpreq = "email=$API_UserName&password=$API_Password&xml=$API_XML";

  //echo "Request was $nvpreq\n\n";

  $httpResponse = Send_CE_XML_Request($nvpreq);

  //echo "Response was $httpResponse\n\n";



  $my_reader = new XMLReader();
  $my_reader->xml($httpResponse);

  $xml_result = "";
  $xml_token = "";
  $xml_errorcode = "";
  $xml_errormsg = "";


  while ($my_reader->read())
  {
      switch ($my_reader->nodeType)
      {
          case XMLReader::ELEMENT:
              if ($my_reader->name == "Result")
              {
                  $my_reader->read();
                  $xml_result = $my_reader->value;
              }
              else if ($my_reader->name == "Token")
              {
                  $my_reader->read();
                  $xml_token = $my_reader->value;
              }
              else if ($my_reader->name == "ErrorCode")
              {
                  $my_reader->read();
                  $xml_errorcode = $my_reader->value;
              }
              else if ($my_reader->name == "ErrorText")
              {
                  $my_reader->read();
                  $xml_errormsg = $my_reader->value;
              }
              
              break;
      }
  }

  if ($xml_result == "Error")
  {
      print "Error - error code was $xml_errorcode, error message was $xml_errormsg\n";
  }
  else if ($xml_result == "Success")
  {
      $API_Token = urlencode($xml_token);

      if ($activity == 'email')
      {
          //$API_XML_String = '<GetCRMActivitiesWebsiteVisitsRequest contact_id="3432" ></GetCRMActivitiesWebsiteVisitsRequest>';
          //$API_XML_String .= "</GetCRMActivitiesRequest>\n";

          //$date = strtotime('-3 month');

          $date = date("Y-m-d", strtotime("-3 month"));
          //echo date("Y-n-j", strtotime("last day of previous month"));

          $API_XML_String = '<GetBroadcastsRequest Datetime_sent="' . $date . ' 12:00:00"></GetBroadcastsRequest>';

          //Datetime_sent="2012-03-01 12:00:00"
          
          //echo "XML Was $API_XML_String<br>\n";
          
          $API_XML = urlencode($API_XML_String);

          // NVPRequest for submitting to server
          $nvpreq = "email=$API_UserName&auth_token=$API_Token&xml=$API_XML";
          
          //$API_XML = urlencode($API_XML_String);

          // NVPRequest for submitting to server
          //$nvpreq = "email=$API_UserName&auth_token=$API_Token&xml=$API_XML";

          //echo "Request was $nvpreq\n\n";

          $httpResponse = Send_CE_XML_Request($nvpreq);        // getting response from server

          //$array = json_decode( json_encode( (array) $httpResponse ), true);

          //var_dump($array);

          $xml = simplexml_load_string($httpResponse);

          return $xml;

          /*foreach($xml->Broadcasts->Broadcast as $broadcast){

            print_r($broadcast->NumberOfClicks);
            print_r($broadcast->NumberOfReads);
            print_r($broadcast->Status);
             [Broadcast_id] 
                              [Status] 
                              [Subject] =&gt; Belay Tuesday: Time to Decline
                              [AccountNumber] =&gt; 33547
                              [GroupID] =&gt; 105
                              [GroupName] =&gt; Actually Useful
                              [NumberOfRecipients] =&gt; 10804
                              [DatetimeQueued] =&gt; 2015-06-02 07:57:38
                              [DatetimeScheduledForDelivery] =&gt; 2015-06-02 08:00:00
                              [DatetimeSent] =&gt; 2015-06-02 08:07:00
                              [RecipientFiltersUsed] =&gt; 
          }*/
          //print_r($xml);
      
          //echo "Response was $httpResponse\n\n";

          //$xml = xml_parser_create();
          //xml_parse_into_struct($xml, $httpResponse, $vals, $index);

          //xml_parser_free($xml);

          //echo "\nIndex array\n";
          //echo 'Site visits - ';
          //print_r($index['WEBSITEVISITS'][0]);
          //echo "\nVals array\n";
          //print_r($index);
          //print_r($vals);
      }

      if ($activity == 'leads'){

          $API_XML_String = '<GetContactsRequest start_chunk_id="0" do_lead_scoring="true"></GetContactsRequest>';
  
          $API_XML = urlencode($API_XML_String);

          // NVPRequest for submitting to server
          $nvpreq = "email=$API_UserName&auth_token=$API_Token&xml=$API_XML";;

          $httpResponse = Send_CE_XML_Request($nvpreq);        

          $xml = simplexml_load_string($httpResponse);

          return $xml;
      }

      /*if ($activity == 'leads'){
        $API_XML_String = '<GetContactsRequest>';
        
        //echo "XML Was $API_XML_String<br>\n";     
        $API_XML = urlencode($API_XML_String);

        // NVPRequest for submitting to server
        $nvpreq = "email=$API_UserName&auth_token=$API_Token&xml=$API_XML";

        //echo "Request was $nvpreq\n\n";

        $httpResponse = Send_CE_XML_Request($nvpreq);        
        
        //echo "Response was $httpResponse\n\n";
        
        
        echo "Response was $httpResponse\n\n";
    }*/

  } else {
      print "Unrecognized XML\n";
  }

}

/**
 * Returns an array of clients
 *
 * use: call <?php get_clients(); ?>
 * @version 1.1
 */
function get_client_list(){

  require (APP_BASE . "/assets/database.php");

  //try query from database
  try {

    $results = $db->prepare("SELECT ID, client_company, primary_email, assigned_team FROM clients"); // WHERE access_level = 0
    $results->execute();

  } catch(Exception $E) {

    echo "Data could not be found.";
    exit();
    
  }

  $clients = $results->fetchAll(PDO::FETCH_ASSOC);

  return $clients;

}

/**
 * Returns an array of clients
 *
 * use: call <?php get_clients(); ?>
 * @version 1.1
 */
function get_clients(){

  require (APP_BASE . "/assets/database.php");

  //try query from database
  try {

    $results = $db->prepare("SELECT ID, client_company, primary_contact, secondary_contact, secondary_email, primary_email, primary_phone, primary_cell, account_status, assigned_team FROM clients"); // WHERE access_level = 0
    $results->execute();

  } catch(Exception $E) {

    echo "Data could not be found.";
    exit();
    
  }

  $clients = $results->fetchAll(PDO::FETCH_ASSOC);

  return $clients;

}
  
  /**
   * Returns an array of user information by ID
   *
   * use: call <?php get_client_single(ID); ?>
   *
   * @param $id  The users ID
   * @version 1.1
   */
  function get_client_single($id){

    require (APP_BASE . "/assets/database.php");

    //try query from database
    try{

      $results = $db->prepare("SELECT client_company, primary_contact, primary_email, primary_phone, primary_cell, secondary_contact, secondary_email, client_address, client_city, client_state, client_zip, client_website, assigned_team, product_sandbox, product_website, product_writing, product_video, product_bizdev FROM clients WHERE ID = :id");
      $results->bindParam(':id', $id);
      $results->execute();


    } catch(Exception $E) {
      echo "Data could not be found.";
      exit();
    }

    $user = $results->fetch(PDO::FETCH_ASSOC);

    return $user;

  }

    /**
     * Used by Administrators to create a new client
     *
     * use: call <?php create_client($company, $primary, $secondary, $email, $address, $city, $state, $zip, $phone, $cell, $website, $google_id, $twilio_id, $twilio_token, $sandbox_id, $sandbox_apikey, $access_level) ?>
     *
     * @param   $company             The client company
     * @param   $primary_contact     The primary contact
     * @param   $primary_email       The secondary contact
     * @param   $primary_phone       The primary email
     * @param   $primary_cell
     * @param   $secondary_contact
     * @param   $secondary_email
     * @param   $address
     * @param   $city
     * @param   $state
     * @param   $zip
     * @param   $website
     * @param   $team
     * @param   $product_sandbox
     * @param   $product_website
     * @param   $product_writing
     * @param   $product_video
     * @param   $product_bizdev
     *
     * @version 1.1
     */
    function create_client($company, $primary_contact, $primary_email, $primary_phone, $primary_cell, $secondary_contact, $secondary_email, $address, $city, $state, $zip, $website, $team, $product_sandbox, $product_website, $product_writing, $product_video, $product_bizdev){

      require(APP_BASE . "/assets/database.php");

      try {

        //$update = $db->prepare($sql);
        $create = $db->prepare("INSERT INTO clients(
                                        client_company,
                                        primary_contact,
                                        primary_email,
                                        primary_phone,
                                        primary_cell,                                       
                                        secondary_contact,
                                        secondary_email, 
                                        client_address, 
                                        client_city, 
                                        client_state, 
                                        client_zip,
                                        client_website,
                                        assigned_team,
                                        product_sandbox,
                                        product_website,
                                        product_writing,
                                        product_video,
                                        product_bizdev
                                        ) VALUES (
                                        :company, 
                                        :primary_contact,
                                        :primary_email,
                                        :primary_phone,
                                        :primary_cell,
                                        :secondary_contact, 
                                        :secondary_email, 
                                        :address, 
                                        :city, 
                                        :state,
                                        :zip,  
                                        :website,
                                        :team,
                                        :product_sandbox,
                                        :product_website,
                                        :product_writing,
                                        :product_video,
                                        :product_bizdev
                              )");
        $create->bindParam(':company', $company,PDO::PARAM_STR);
        $create->bindParam(':primary_contact', $primary_contact, PDO::PARAM_STR);
        $create->bindParam(':primary_email', $primary_email, PDO::PARAM_STR);
        $create->bindParam(':primary_phone', $primary_phone, PDO::PARAM_STR);
        $create->bindParam(':primary_cell', $primary_cell, PDO::PARAM_STR);
        $create->bindParam(':secondary_contact', $secondary_contact, PDO::PARAM_STR);
        $create->bindParam(':secondary_email', $secondary_email, PDO::PARAM_STR);
        $create->bindParam(':address', $address, PDO::PARAM_STR);
        $create->bindParam(':city', $city, PDO::PARAM_STR);
        $create->bindParam(':state', $state, PDO::PARAM_STR);
        $create->bindParam(':zip', $zip, PDO::PARAM_STR);
        $create->bindParam(':website', $website, PDO::PARAM_STR);
        $create->bindParam(':team', $team, PDO::PARAM_STR);
        $create->bindParam(':product_sandbox', $product_sandbox, PDO::PARAM_STR);
        $create->bindParam(':product_website', $product_website, PDO::PARAM_STR);
        $create->bindParam(':product_writing', $product_writing, PDO::PARAM_STR);
        $create->bindParam(':product_video', $product_video, PDO::PARAM_STR);
        $create->bindParam(':product_bizdev', $product_bizdev, PDO::PARAM_STR);
        $create->execute();

      } catch(Exception $e) {

        echo 'Sorry no dice';
        return false;
        exit();

      }

      $GLOBALS['msg']['user_was_updated'] = true;

    }
      /**
       * Update existing client
       *
       * @param   $id                  The client ID
       * @param   $company             The client company
       * @param   $primary             The primary contact
       * @param   $secondary           The secondary contact
       * @param   $email               The primary email
       * @param   $address
       * @param   $city
       * @param   $state
       * @param   $zip
       * @param   $phone
       * @param   $cell
       * @param   $website
       * @param   $google_id
       * @param   $twilio_id
       * @param   $twilio_token
       * @param   $sandbox_id
       * @param   $sandbox_apikey
       * @param   $access_level            
       * 
       * @version 1.1
       */
      //function update_client($id, $product, $description, $price){
      function update_client($id, $company, $primary_contact, $primary_email, $primary_phone, $primary_cell, $secondary_contact, $secondary_email, $address, $city, $state, $zip, $website, $team, $product_sandbox, $product_website, $product_writing, $product_video, $product_bizdev){


        require(APP_BASE . "/assets/database.php");

        //update statement
        $sql = "UPDATE `clients` SET 
                    `client_company` = :company,
                    `primary_contact` = :primary_contact,
                    `primary_email` = :primary_email,
                    `primary_phone` = :primary_phone,
                    `primary_cell` = :primary_cell,                                       
                    `secondary_contact` = :secondary_contact,
                    `secondary_email` = :secondary_email, 
                    `client_address` = :address, 
                    `client_city` = :city, 
                    `client_state` = :state, 
                    `client_zip` = :zip,
                    `client_website` = :website,
                    `assigned_team` = :team,
                    `product_sandbox` = :product_sandbox,
                    `product_website` = :product_website,
                    `product_writing` = :product_writing,
                    `product_video` = :product_video,
                    `product_bizdev` = :product_bizdev
              WHERE ID = :id";

        try {

          $update = $db->prepare($sql);          
          $update->bindParam(':company', $company, PDO::PARAM_STR);
          $update->bindParam(':primary_contact', $primary_contact, PDO::PARAM_STR);
          $update->bindParam(':primary_email', $primary_email, PDO::PARAM_STR);
          $update->bindParam(':primary_phone', $primary_phone, PDO::PARAM_STR);
          $update->bindParam(':primary_cell', $primary_cell, PDO::PARAM_STR);
          $update->bindParam(':secondary_contact', $secondary_contact, PDO::PARAM_STR);
          $update->bindParam(':secondary_email', $secondary_email, PDO::PARAM_STR);
          $update->bindParam(':address', $address, PDO::PARAM_STR);
          $update->bindParam(':city', $city, PDO::PARAM_STR);
          $update->bindParam(':state', $state, PDO::PARAM_STR);
          $update->bindParam(':zip', $zip, PDO::PARAM_STR);
          $update->bindParam(':website', $website, PDO::PARAM_STR);
          $update->bindParam(':team', $team, PDO::PARAM_STR);
          $update->bindParam(':product_sandbox', $product_sandbox, PDO::PARAM_STR);
          $update->bindParam(':product_website', $product_website, PDO::PARAM_STR);
          $update->bindParam(':product_writing', $product_writing, PDO::PARAM_STR);
          $update->bindParam(':product_video', $product_video, PDO::PARAM_STR);
          $update->bindParam(':product_bizdev', $product_bizdev, PDO::PARAM_STR);
          $update->bindParam(':id', $id, PDO::PARAM_STR);
          $update->execute();

        } catch(Exception $e) {

          echo 'Epic Fail..'; 
          return false;
          exit();

        }

        $GLOBALS['msg']['user_was_updated'] = true;

      }

/**
 * Returns an array of users
 *
 * use: call <?php get_users(); ?>
 * @version 1.1
 */
function get_users(){

  require (APP_BASE . "/assets/database.php");

  //try query from database
  try {

    $results = $db->prepare("SELECT ID, first_name, last_name, user_email, user_team, user_phone, account_status, access_level, user_gravatar FROM users WHERE access_level = 0");
    $results->execute();

  } catch(Exception $e) {

    echo "Data could not be found.";
    exit();
    
  }

  $users = $results->fetchAll(PDO::FETCH_ASSOC);

  return $users;

}

  /**
   * Returns an array of user information by ID
   *
   * use: call <?php get_user_single(ID); ?>
   *
   * @param $id  The users ID
   * @version 1.1
   */
  function get_user_single($id){

    require (APP_BASE . "/assets/database.php");

    //try query from database
    try{

      $results = $db->prepare("SELECT first_name, last_name, user_email, user_phone, user_gravatar, account_status FROM users WHERE ID = :id");
      $results->bindParam(':id', $id);
      $results->execute();


    } catch(Exception $E) {
      echo "Data could not be found.";
      exit();
    }

    $user = $results->fetch(PDO::FETCH_ASSOC);

    return $user;

  }

    /**
     * Returns an array of users
     *
     * use: call <?php get_users(); ?>
     * @version 1.1
     */
    function get_user_config($id){

      require (APP_BASE . "/assets/database.php");

      //try query from database
      try{

        $results = $db->prepare("SELECT google_id, sandbox_username, sandbox_pass, wrike_id FROM users WHERE ID = :id");
        $results->bindParam(':id', $id);
        $results->execute();


      } catch(Exception $E) {
        echo "Data could not be found.";
        exit();
      }

      $user = $results->fetch(PDO::FETCH_ASSOC);

      return $user;

     }

        /**
         * Update user config google analytics id, sandbox user, sandbox password
         *
         * use: call <?php update_user_config; ?>
         * @version 1.1
         */
        //function update_user_config($id, $google, $username, $pass, $wrike){
        function update_user_config($id, $google, $username, $pass){

          require (APP_BASE . "/assets/database.php");

          //update statement
          // `wrike_id` = :wrike_id
          $sql = "UPDATE `users` SET 
                      `google_id` = :google_id, 
                      `sandbox_username` = :sandbox_username,
                      `sandbox_pass` = :sandbox_pass
                     
                WHERE ID = :id";

          try {

            $update = $db->prepare($sql);
            $update->bindParam(':google_id', $google, PDO::PARAM_STR);
            $update->bindParam(':sandbox_username', $username, PDO::PARAM_STR);
            $update->bindParam(':sandbox_pass', $pass, PDO::PARAM_STR);
            //$update->bindParam(':wrike_id', $wrike, PDO::PARAM_STR);
            $update->bindParam(':id', $id, PDO::PARAM_STR);            
            $update->execute();

          } catch(Exception $e) {

            echo 'Epic Fail..'; 
            return false;
            exit();

          }

          $GLOBALS['msg']['user_was_updated'] = true;

         }

/**
 * Returns an array of user information by ID
 *
 * use: call <?php get_lasso; ?>
 *
 * @param $id  The users ID
 * @version 1.1
 */
function get_lasso($id){

  require (APP_BASE . "/assets/database.php");

  //try query from database
  try{

    $results = $db->prepare("SELECT twilio_id, twilio_token FROM users WHERE sku = :id");
    $results->bindParam(':id', $id);
    $results->execute();

    echo "Soo sorry, you suck and so does your code.";

  } catch(Exception $E) {
    echo "Data could not be found.";
    exit();
  }

  $lasso = $results->fetch(PDO::FETCH_ASSOC);

  // Get the PHP helper library from twilio.com/docs/php/install
  require_once(APP_BASE . "/assets/twilio.php"); // Loads the library
   
  // Your Account Sid and Auth Token from twilio.com/user/account
  $sid = $lasso[0]; 
  $token = "{{ $lasso[1] }}"; 
  $client = new Services_Twilio($sid, $token);

  return $client;

}

function export_as_csv($title, $date_created){
  //Connect to Prospect Database
  include_once('prospectDB.php');

  $sql = "SELECT name, phone, email, address, city, state, zip FROM prospects WHERE ";

  //create file name
  $filename = $title_of_search . $date_created . '.csv'; 

  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: text/csv");

  $display = fopen("php://output", 'w');

  $flag = false;
  while($row = mysql_fetch_assoc($qur)) {
    if(!$flag){
      //display field/column names as first row
      fputcsv($display, array_keys($row), ",", '"');
      $flag = true;
    }
    fputcsv($display, array_values($row), ",", '"');
  }

  fclose($display);

}

/**
 * Converts a number of special characters into their HTML entities.
 *
 * Specifically deals with: &, <, >, ", and '.
 *
 * $quote_style can be set to ENT_COMPAT to encode " to
 * &quot;, or ENT_QUOTES to do both. Default is ENT_NOQUOTES where no quotes are encoded.
 *
 * @since 1.2.2
 * @access private
 *
 * @param string $string The text which is to be encoded.
 * @param int $quote_style Optional. Converts double quotes if set to ENT_COMPAT, both single and double if set to ENT_QUOTES or none if set to ENT_NOQUOTES. Also compatible with old values; converting single quotes if set to 'single', double if set to 'double' or both if otherwise set. Default is ENT_NOQUOTES.
 * @param string $charset Optional. The character encoding of the string. Default is false.
 * @param boolean $double_encode Optional. Whether to encode existing html entities. Default is false.
 * @return string The encoded text with HTML entities.
 */
function _wp_specialchars( $string, $quote_style = ENT_NOQUOTES, $charset = false, $double_encode = false ) {
  $string = (string) $string;

  if ( 0 === strlen( $string ) )
    return '';

  // Don't bother if there are no specialchars - saves some processing
  if ( ! preg_match( '/[&<>"\']/', $string ) )
    return $string;

  // Account for the previous behaviour of the function when the $quote_style is not an accepted value
  if ( empty( $quote_style ) )
    $quote_style = ENT_NOQUOTES;
  elseif ( ! in_array( $quote_style, array( 0, 2, 3, 'single', 'double' ), true ) )
    $quote_style = ENT_QUOTES;

  // Store the site charset as a static to avoid multiple calls to wp_load_alloptions()
  /*if ( ! $charset ) {
    static $_charset;
    if ( ! isset( $_charset ) ) {
      $alloptions = wp_load_alloptions();
      $_charset = isset( $alloptions['blog_charset'] ) ? $alloptions['blog_charset'] : '';
    }
    $charset = $_charset;
  }*/

  //$charset = 'utf8';

  //if ( in_array( $charset, array( 'utf8', 'utf-8', 'UTF8' ) ) )
  $charset = 'UTF-8';

  $_quote_style = $quote_style;

  if ( $quote_style === 'double' ) {
    $quote_style = ENT_COMPAT;
    $_quote_style = ENT_COMPAT;
  } elseif ( $quote_style === 'single' ) {
    $quote_style = ENT_NOQUOTES;
  }

  // Handle double encoding ourselves
  if ( $double_encode ) {
    $string = @htmlspecialchars( $string, $quote_style, $charset );
  } else {
    // Decode &amp; into &
    $string = wp_specialchars_decode( $string, $_quote_style );

    // Guarantee every &entity; is valid or re-encode the &
    $string = wp_kses_normalize_entities( $string );

    // Now re-encode everything except &entity;
    $string = preg_split( '/(&#?x?[0-9a-z]+;)/i', $string, -1, PREG_SPLIT_DELIM_CAPTURE );

    for ( $i = 0, $c = count( $string ); $i < $c; $i += 2 ) {
      $string[$i] = @htmlspecialchars( $string[$i], $quote_style, $charset );
    }
    $string = implode( '', $string );
  }

  // Backwards compatibility
  if ( 'single' === $_quote_style )
    $string = str_replace( "'", '&#039;', $string );

  return $string;
}