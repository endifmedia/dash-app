<?php
/**
 * Front to the dash pplication. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package dashboard app
 */

/** Loads the dash app */
require( dirname( __FILE__ ) . '/dashboard.php' );