<?php 
/**
 * Footer template 
 *
 * Used to display the page footer 
 * @version 1.1 dashboard app
 */
?>
          </div><!-- end responsive -->
        </div><!-- end col sm-9 -->
      </div><!-- end container-fluid -->
    </div><!-- end row -->

    <!-- jQuery 
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <!-- Core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="assets/js/jquery.md5.js"></script>
    <script type="text/javascript" src="assets/js/jquery.easy-pie-chart.js"></script>
    <script type="text/javascript" src="assets/js/chartist.min.js"></script>




    <script src="assets/js/bootstrap.min.js"></script>

    <!-- FormValidation plugin and the class supports validating Bootstrap form -->
    <!-- <script src="/vendor/formvalidation/dist/js/formValidation.min.js"></script> -->
    <script src="assets/js/formvalidation/dist/js/framework/bootstrap.min.js"></script>

    
		<script type="text/javascript">

        $(document).ready(function() {        
          $('a.delete').on('click', function(e) {
            e.preventDefault();
            var parent = $(this).parent().parent();
            var id = $(this).attr('id').split('-');
            var table = jQuery('.page-header a').attr('id').split('-');
            var data = 'id=' + id[1] + '&table=' + table[1];

            $.ajax({
              type: 'POST',
              url: 'delete.php',
              dataType: 'text',
              data: data,

              beforeSend: function(data) {                
                parent.children().css('backgroundColor', 'rgb(219, 115, 115)' ).animate(300);
              },
              success: function() {
                console.log(data);
                parent.slideUp(1000,function() {
                  parent.remove();
                });
              }
            });
          });
          
          $('a > .addnew-submit').on('click', function(e) {
            e.preventDefault();

            /*var fname = $('#product_name').val();
            var lname = 
            var email = (expression)
            var email = 
            var username =
            var password = 
            var user_type = */

           /* var description = $('#product_description').val();
            var unitprice = $('#unit_price').val();*/

            var data = 'product_name=' + input + '&product_description=' + description + '&unit_price=' + unitprice;

            $.ajax({
              type: 'POST',
              url: 'insert.php',
              dataType: 'text',
              data: data,

              beforeSend: function(data) {                
                //parent.children().css('backgroundColor', 'rgb(219, 115, 115)' ).animate(300);
                //alert('Success!');
              },
              success: function() {
                console.log(data);
                //parent.slideUp(1000,function() {
                  //parent.remove();
                //});
                $.fancybox(
                      '<p>Data Saved</p><p><a href="products.php"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-file-pdf-o"> Return to Products</i></button></a>',{
                          padding:15,
                          closeBtn:true
                      }
                );
              }
            });
          });

          var CurrPage = document.location.href.split('/').pop();

            switch(CurrPage){

            case 'dashboard.php':
             $('#li_home').addClass('active');
             break;

            case 'analytics.php':
             $('#li_analytics').addClass('active');
             break;

            case 'sandbox.php?page=email':
             $('#li_calls').addClass('active');
             break;

            case 'wrike.php':
             $('#li_social').addClass('active');
             break;

            case 'settings.php':
             $('#li_config').addClass('active');
             break;

            }
        });

        jQuery('#emailaddress').blur( function(){  

          t = jQuery('#emailaddress').val();
          md5 = $.md5(t);
          //gravatar_image = 'http://www.gravatar.com/avatar/' + md5 + '?s=20&amp;d=mm&amp;r=g';

          //update image src
          jQuery('#display-gravatar').attr('src', 'http://www.gravatar.com/avatar/' + md5 + '?s=140&d=mm&r=g');

          //update input box
          jQuery('#gravatar').attr('value', 'http://www.gravatar.com/avatar/' + md5);


        });

        $(function() {
            $('.chart').easyPieChart({
                //your configuration goes here
            });
        });

        var data = {
          labels: ['Direct', 'Seach', 'Graphs', 'Meetings'],
          series: [5, 5, 10, 80]
        };

        var options = {
          labelInterpolationFnc: function(value) {
            return value[0]
          }
        };

        var responsiveOptions = [
          ['screen and (min-width: 640px)', {
            chartPadding: 30,
            labelOffset: 100,
            labelDirection: 'explode',
            labelInterpolationFnc: function(value) {
              return value;
            }
          }],
          ['screen and (min-width: 1024px)', {
            labelOffset: 80,
            chartPadding: 20
          }]
        ];

        new Chartist.Pie('.traffic', data, options, responsiveOptions);
        
    </script>
    

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="assets/js/ie10-viewport-bug-workaround.js"></script> -->
  </body>
</html>
