<?php 
/**
 * Call History template 
 *
 * Used to display the call history page 
 * @version 1.1 dashboard app
 */

 include('dashboard-header.php'); 
 
?>

          <h1 class="page-header">Call History</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                <th>Who</th>
                  <th>Rep</th>
                  <th>Time on Call</th>
                  <th>Sale?</th>
                  <th>Goals</th>
                  <th>Success (1-10)</th>
                  <th> - </th>                 
                </tr>
              </thead>
              <tbody>
                <tr>

                <?php 

                  $client = get_lasso($_SESSION['userid']);

                  // Loop over the list of records and echo a property for each one
                  foreach ($client->account->usage_records->getIterator(0, 50, array(
                          "Category" => "calls-inbound",
                          "StartDate" => date('Y-m-d'),
                          "EndDate" => "2012-09-30"
                      )) as $record) {

                      echo '<td>' . $record->price . '</td>';

                  } 


                ?>

          
                  
                  <!--<td>Gordon Sheller</td>
                  <td>Athena (id 45)</td>
                  <td>1:25 mins</td>
                  <td>NO</td>
                  <td>32</td>
                  <td>5</td>
                  <td>
                    <button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" title="edit <?php echo strtolower($page); ?>"> Edit</i></button> 
                    <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-times" title="delete <?php echo strtolower($page); ?>"> Delete</i></button>
                  </td>  -->               
                </tr>          
              </tbody>
            </table>

<?php include('dashboard-footer.php'); ?>